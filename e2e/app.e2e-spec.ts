import { Ng2MonitorPage } from './app.po';

describe('ng2-monitor App', function() {
  let page: Ng2MonitorPage;

  beforeEach(() => {
    page = new Ng2MonitorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
