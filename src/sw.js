console.log('Started', self);

self.userId = null;
self.token = null;
//self.backendUrl = 'https://iot-backend-monitor.herokuapp.com';
//self.backendUrl = 'http://192.168.0.115:1337'
self.backendUrl = 'http://localhost:1337';
self.addEventListener('install', function (event) {
  self.skipWaiting();
  console.log('Installed', event);
});
self.addEventListener('activate', function (event) {
  console.log('Activated', event);
});
self.addEventListener('push', function (event) {
  console.log('Push message received', event);

  //Fetch the notificacion
  var url = self.backendUrl + '/user/' + self.userId + '/notifications?limit=1&sort=createdAt DESC';
  event.waitUntil(
    fetch(url, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'access_token': self.jwt
      }
    }).then(function (res) {
      console.log('body', res.body);
      console.log('before json', res);

      return res.json();
    }).then(function (res) {
      if (res.error || !res.length > 0) {
        console.error('The API returned an error.', res.error);
        throw new Error();
      }
      console.log('We are getting there');
      console.log(res);
      var res = res[0];
      console.log('title', res.title);

      self.registration.showNotification(res.title, {
        body: res.body,
        data: res.url,
        tag: res.id
      })
    }).catch(function (err) {
      console.error(err);
    })
  )


});

self.addEventListener('message', function (event) {
  var data = JSON.parse(event.data);

  console.log('SW received a message');
  console.log(data);

  if ('uid' in data) {
    self.userId = data.uid;
  }
  if ('token' in data) {
    self.jwt = data.token;
  }
})
