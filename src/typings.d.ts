///<reference path="./service.worker.d.ts"/>

// Typings reference file, see links for more information
// https://github.com/typings/typings
// https://www.typescriptlang.org/docs/handbook/writing-declaration-files.html

declare var System: any;

declare let Materialize: any;

/**
 * Service Workers window variables
 */
declare let ServiceWorkerRegistration: ServiceWorkerRegistration;
declare let Notification: Notification;

import {Observable} from 'rxjs';


interface SailsObject {
    id: number,
    createdAt?: Date,
    updatedAt?: Date
}

interface User {
	firstName?: string
	lastName: string
	email: string
	password: string 
}

interface Sensor {
    id: number
    name: string
    alert?: boolean
    thing: number
    measures: Array<any>
    lastMeasure?: any
    unit?: any
    createdAt: Date
    updatedAt: Date

}

interface Thing {
    id: number
    name?: string
    domain?: string
    ip?: string
    mac?: string
    alert?: boolean
    sensors: Array<Sensor> | Observable<Sensor[]>
    createdAt: Date
    updatedAt: Date
}

interface Group extends SailsObject {
    name: string,
    things?: Thing[] | Observable<Thing[]>,
    users?: User[]
}

