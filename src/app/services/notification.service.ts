import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AuthService } from './auth.service';
import { SailsService } from './sails.service';

@Injectable()
export class NotificationService {

  notifications$: Subject<any> = new BehaviorSubject<any>([]);
  notifications: Array<any> = [];
  userId: number;

  constructor(
    private authService: AuthService,
    private sailsService: SailsService
  ) {
    this.userId = this.authService.getId();
    this.loadNotifications();
    this.sailsService.on('notification')
      .subscribe(
      res => {
        if (res.verb === 'created') {
          this.notifications.push(res.data);
          this.notifications$.next(this.notifications);
        }
      }
      )
  }

  getNotification(id: number) {
    return this.sailsService.get(`/notification/${id}?populate=[]`);
  }

  getNotifications(limit?: number) {
    this.loadNotifications(limit);
    return this.notifications$.asObservable();
  }

  private loadNotifications(limit?: number) {
    limit = limit || 30;
    let url = `/user/${this.userId}/notifications?limit=${limit}&sort=createdAt DESC`
    this.sailsService.get(url)
      .subscribe(notifications => {

        this.notifications = notifications;
        this.notifications$.next(notifications);
      }, error => {
        console.error('error getting the notifications' + error);
      })
  }

  setAsRead(id: number) {
    return this.sailsService.put(`/notification/${id}`, { read: true });
  }



}
