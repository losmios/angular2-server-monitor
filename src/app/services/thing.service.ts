import { Injectable } from '@angular/core';
import { SailsService } from './sails.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { Observer, Observable } from 'rxjs';
import { GroupService } from './group.service';
declare let io: any;
import { Thing, Sensor, Group, Unit } from '../server-monitor.models';
import { AuthService } from './auth.service';

/**
 * TODO:
 * TClean up this mess with the groups
 */

interface GroupThings {
  group: number,
  things: Thing[]
}

interface GroupThingsObservable {
  group: number,
  things$: Subject<Thing[]>
}

@Injectable()
export class ThingService {

  private groupThings$: GroupThingsObservable[] = [];
  private groupThings: GroupThings[] = [];

  private things$: Subject<Thing[]> = new BehaviorSubject<Thing[]>([]);
  private things: Thing[];
  //sensors$: Subject<Array<Sensor>> = new BehaviorSubject<Array<Sensor>>(null);
  sensors$: Subject<any> = new BehaviorSubject<any>([]);
  sensors: any = [];

  private joinedGroups: Array<Group>;

  private sensor$: Subject<Sensor> = new Subject<Sensor>();

  private units: Unit[] = [];

  data = {};

  lastThingSocket: any;

  constructor(
    private sailsService: SailsService,
    private authService: AuthService) {
    console.log('getting the things');
    this.getJoinedGroups().subscribe(groups => {
      this.joinedGroups = groups;
    });
    this.getAllThings();
    this.authService.isAuth().subscribe(
      isAuth => {
        this.startListeners();
      }
    )
    this.sailsService.get('/unit').subscribe(
      (units: Unit[]) => {
        this.units = units;
      }
    )

  }

  private startListeners() {
    console.log('starting to listen');
    this.sailsService.on('thing').subscribe(
      (resData) => {
        if (JSON.stringify(resData) === JSON.stringify(this.lastThingSocket)) {
          return;
        }
        this.lastThingSocket = resData;
        console.log('listening on thing socket', resData);
        let verb = resData.verb;
        let obj = resData.data;
        if (verb == 'created') {
          this.sailsService.get('/thing/' + obj.id)
            .subscribe(
            t => {
              this.things.push(t);
              this.things$.next(this.things);
            }
            )
          return;
        }
        if (verb === 'updated') {
          this.things = this.things.map(
            o => {
              //Check this because it's failing gracefully
              if (!('id' in resData)) {
                console.error('there is no id in data', resData);
              }
              if (o.id === resData.id) {
                o = Object.assign(o, resData.data);
              }
              return o;
            }
          )
          this.things$.next(this.things);
          return;
        }
        if (verb === 'destroyed') {
          this.things = this.things.filter(t => t.id !== resData.id);
          return;
        }
      },
      (error) => {
        console.error('Error on thing SailsEvent', error);
      },
      () => {
        console.log('complete subscription to "thing" event');
      }
    )

    //Dummy get to subscribe to the things socket
    this.sailsService.get('/thing').subscribe(things => {
      console.log('Dummy things get when the things updates', things);
    }, error => {
      console.error(error);
    });
    var sensors = this.sensors;

    this.sailsService.on('sensor').subscribe(
      resData => {
        let verb = resData.verb;
        switch (verb) {
          case 'addedTo': {
            if (resData.attribute == 'measures') {
              console.log(sensors);
              let sensor = this.sensors.find(sensor => sensor.id === resData.id);
              if (!sensor) {
                console.error('Measure read from socket but no matching sensor', sensor);
              }
              this.sailsService.get('/measure/' + resData.addedId).subscribe(
                measure => {
                  delete measure.sensor;
                  sensor.lastMeasure = measure;
                  this.sensor$.next(sensor);
                }
              )
            }
            break;
          }
          case 'updated':
            console.log('sensor ALERT ****************');
            console.log(resData);
            let sensorId = resData.id;
            if (typeof resData.data.alert !== 'undefined') {
              let sensor = this.sensors.filter(s => s.id === sensorId)[0];
              sensor.alert = resData.data.alert;
              this.sensor$.next(sensor);
            }
            break;
        }
      },
      (error) => {
        console.error('Error on sensor SailsEvent', error);
      },
      () => {
        console.log('complete subscription to "sensor" event');
      }
    )
  }


  getThings(groupId?: number) {
    if (groupId) {
      return this.groupThings$.find(gt => gt.group === groupId).things$.asObservable();
    }
    return this.things$.asObservable();
  }

  private getAllThings() {
    let url = '/thing';
    //Get the groups
    this.getGroups()
      .subscribe(
      groups => {
        if (!groups.length) {
          console.warn("The user doesn't have any groups");
        }

        let query = "?where=" + JSON.stringify({
          group: groups.map(g => g.id)
        });

        //get the things
        this.sailsService.get(url + query).subscribe(
          things => {

            this.things = things;
            things.map(
              thing => {
                thing.sensors.map(
                  sensor => {
                    this.sensors.push(sensor);
                  }
                )

                if (!this.groupThings.find(gt => gt.group === thing.group)) {
                  this.groupThings.push({
                    group: thing.id,
                    things: [thing]
                  })
                } else {
                  this.groupThings = this.groupThings.map(
                    gt => {
                      if (gt.group === thing.group) {
                        gt.things.push(thing);
                      }
                      return gt;
                    }
                  )
                }

              }
            )
            //send the data to all subscribers
            this.groupThings.map(
              gt => {
                this.groupThings$.push({
                  group: gt.group,
                  things$: new BehaviorSubject<Thing[]>(gt.things)
                })
              }
            )
            console.log('sensors fetched', this.sensors);
            this.things$.next(things);

            this.loadUnits2();
          },
          error => {
            console.error('Error in getAllThings', error);
          },
          () => {
            this.startListeners()
          }
        )
      }
      )



    return this.things$.asObservable();
  }

  getSensor(id: number) {
    return this.sensor$.asObservable().find(sensor => sensor.id === id);
  }

  getSensorObservable() {
    return this.sensor$.asObservable();
  }

  attachSensors(sensors: Sensor[]) {
    this.getSensorObservable()
      .subscribe(sensor => {
        if (!(sensor => sensor.id in sensors.map(s => s.id))) {
          return;
        }
        sensors = sensors.map(s => {
          if (s.id === sensor.id) {
            return Object.assign(sensor, s);
          }
          return s;
        })
      }, error => {
        console.error('Error getting the sensor sockets', error);
      })
  }

  private loadSensors(things: Thing[]) {
    things.map(thing => {
      let sensors_array = [];
      thing.sensors.map(sensor => {
        sensors_array.push(sensor);
      })
      let ok = {
        thingId: thing.id,
        sensors$: new BehaviorSubject<any>(sensors_array),
        sensors: sensors_array
      }
      this.sensors.push(ok);
    })
  }

  private loadUnits(): Observable<Sensor> {
    let subject = new Subject<Sensor>();
    let count = this.sensors.length;
    this.sensors.map(
      sensor => {
        this.sailsService.get('/unit/' + sensor.lastMeasure.unit).subscribe(
          unit => {
            sensor.lastMeasure.unit = unit;
            subject.next(sensor);
            count--;
            if (count === 0) {
              subject.complete();
            }
            return sensor;
          }
        )
      }
    )
    return subject.asObservable()
  }

  private loadUnits2(): void {
    this.sensors.map((sensor: Sensor) => {
      let unit = this.units.find(u => u.id === sensor.lastMeasure.unit);
      if (!unit) {
        this.getUnit(sensor.lastMeasure.unit)
          .subscribe(newUnit => {
            sensor.lastMeasure.unit = newUnit;
            this.sensor$.next(sensor);
          })
      } else {
        sensor.lastMeasure.unit = unit;
        this.sensor$.next(sensor);
      }
    })
  }

  /**
   * Groups Section
   */

  getGroup(id: number) {
    return this.sailsService.get('/group/' + id);
  }

	/**
	 * Gets the all the groups the user is subscribed to
	 */
  getGroups() {
    let userId = this.authService.getId();
    if (userId) {
      return this.sailsService.get('/user/' + userId + '/groups');
    }
    console.log('user not provided');
    return Observable.throw({
      type: 'Not Authorized',
      message: 'User not provided'
    })
  }


  getPublicGroups() {
    return this.sailsService.get('/group');
  }


	/**
	 * Creates a group
	 * @param name - Name of the group to create
	 */
  createGroup(name: string) {
    return this.sailsService.post('/group/create', { name: name });
  }


	/**
	 * Find a group that contains the query
	 * @param name - is the name of the group to find
	 * @example query - {
			where: {
				name: {
					contains : "Nestor"
				}
			}
		}
	 */
  findGroup(query, populate?: boolean) {
    populate = populate || true;
    return this.sailsService.get('/group', query)
      .do(
      (groups) => {

        if (populate) {
          if (groups.length > 1) {
            groups = groups.map(
              g => {
                g.things = this.getThings(g.id);
                return g;
              }
            )
          } else {
            groups.things = this.getThings(groups.id);
          }
        }

      }
      )
  }

  findOneGroup(name: string) {
    let query = JSON.stringify({ name: name });
    console.log('/group?user=' + query);
    return this.sailsService.get('/group?where=' + query);
  }

  findGroupByName(name: string) {
    let query = JSON.stringify({
      name: {
        contains: name
      }
    })
    return this.sailsService.get('/group?where=' + query);
  }

  joinGroup(groupId: number, permissions?: string[]) {
    let data = {
      user: this.authService.getId()
    }
    return this.sailsService.post('/group/' + groupId + "/join", data)
      .do(group => {
        this.getJoinedGroups().subscribe();
      })
  }

  getJoinedGroups() {
    return this.sailsService.get('/user/' + this.authService.getId() + '/groups')
      .do(groups => {
        this.joinedGroups = groups;
      })
  }

  hasGroups() {
    if (this.joinedGroups.length > 0) {
      return true;
    } else {
      return false
    }
  }

  getGroupAdmins(groupId: number) {
    return this.sailsService.get('group/' + groupId + '/admins');
  }

  getUserPermissions(groupId: number) {
    return this.sailsService.get('/grouppermission?user=' + this.authService.getId() + '&group=' + groupId)
      .map(
      gp => {
        console.log('user: ' + this.authService.getId() + ' for group ' + groupId + 'has these permissions: ', gp);
        return gp[0].permissions;
      });
  }

  getGroupMembers(groupId: number) {
    return this.sailsService.get('/group/' + groupId + '/members').map(
      users => {
        return users.map(u => {
          u['email'] = u.auth.email;
          return u;
        })
      }
    )


  }

  getAdminGroups() {
    let userId = this.authService.getId();
    if (userId) {
      let query = {
        user: userId
      }
      return this.sailsService.get('/grouppermission', query)
        .map(
        gp => {
          let adminGroups = gp.filter(g => g.permissions && g.permissions.indexOf('admin') !== -1);
          return adminGroups.map(g => g.group);
        }
        )
    }
    console.log('user not provided');
    return Observable.throw({
      type: 'Not Authorized',
      message: 'User not provided'
    })
  }

  getUnit(unitId: number): Observable<Unit> {
    let unit = this.units.find(u => u.id === unitId);
    if (unit) {
      return Observable.of(unit);
    }
    return this.sailsService.get('/unit/' + unitId)
      .do(unit => {
        this.units.push(unit);
      })
  }

  updateMember(groupId: number, member: number, status?: string, permissions?: string[]) {
    let subject = new Subject<any>();
    if (!status && !permissions) {
      setTimeout(() => subject.error('nor status or permissions provided'), 0);
    }
    this.sailsService.get('/grouppermission', {
      group: groupId,
      user: member
    }).subscribe(gp => {
      gp = gp[0];
      this.sailsService.put('/grouppermission/' + gp.id, {
        status: status
      }).subscribe(
        gp => {
          subject.next(gp);
          subject.complete();
        }, error => {
          subject.error(error);
          subject.complete();
        }
        )
    }, error => {
      subject.error(error);
      subject.complete();
    })
    return subject.asObservable();
  }

  memberPutPermissions(groupId: number, userId: number, permissions: string[]) {
    let subject = new Subject<any>();
    let query = {
      group: groupId,
      user: userId
    }
    this.sailsService.get('/grouppermission', query).subscribe(
      gp => {
        let id = gp[0].id;
        this.sailsService.put('/grouppermission/' + id, { permissions: permissions }).subscribe(
          gp => {
            return subject.next(gp);
          }, error => {
            subject.error(error);
          }
        )
      }, error => {
        subject.error(error);
      }
    )
    return subject.asObservable();
  }

  removeFromGroup(groupId: number, userId: number) {

    return this.sailsService.delete('/group/' + groupId + '/users/' + userId);

  }

  updateThing(thingId: number, data) {
    return this.sailsService.put('/thing/' + thingId, data);
  }

  getMeasures(sensors: Array<number>, since?: Date, to?: Date) {
    to = to || new Date() //Defaults to right now
    since = since || new Date(to.toJSON().slice(0, 10)) //Defaults to the same day as to but at the beggining
    let query = {

      sensor: sensors,
      createdAt: {
        '>': since.toJSON(),
        '<': to.toJSON()
      }
    }
    let queryString = JSON.stringify(query);
    return this.sailsService.get('/measure?limit=100000&populate=["unit"]&where=' + queryString);
  }


  /**Incidents */
  getIncidents(query: any) {
    if (!('type' in query) || !query.type) {
      query['type'] = 'fail';
    }
    return this.sailsService.get('/incident', query);
  }


}
