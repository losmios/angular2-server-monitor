import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class NavbarService {

  private title = new BehaviorSubject<string>('Monitor')

  constructor() { }

  getTitle() {
    return this.title.asObservable()
  }

  setTitle(t: string) {
    this.title.next(t)
  }

}
