import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { DeviceService } from './device.service';
import { AuthService } from './auth.service';

declare var ServiceWorkerRegistration: ServiceWorkerRegistration;
declare var Notification: Notification;
declare var chrome: any;

@Injectable()
export class ServiceWorkerService {

  isNotificationSupported: Subject<boolean> = new BehaviorSubject<boolean>(false);
  notificationPermission: Subject<string> = new BehaviorSubject<string>(null);
  isPushMessagingSupported: Subject<boolean> = new BehaviorSubject<boolean>(false);
  canReceiveNotifications: Subject<boolean> = new BehaviorSubject<boolean>(true);
  isSubscribed: Subject<boolean> = new BehaviorSubject<boolean>(true);

  swRegistration: ServiceWorkerRegistration;

  constructor(
    private deviceService: DeviceService,
    private authService: AuthService
  ) {
    this.authService.isAuth().subscribe(
      isAuth => {
        if (isAuth) {
          this.sendTokenToSW();
        }
      }
    )
    this.authService.getJwtObservable()
      .subscribe(jwt => {
        if (jwt) {
          this.sendTokenToSW();
        }
      }, error => {
        console.error('error on ServiceWorker.service getting the jwt from the auth.service', error);
      })
  }

  registerServiceWorker(path?: string) {
    path = path || 'sw.js';
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register(path)
        .then((reg: ServiceWorkerRegistration) => {
          console.log(':^)', reg);
          this.swRegistration = reg;
          this.initializeNotifications(reg);

        }).catch((err) => {
          console.log(':^( error registering the service worker', err)
        })
    } else {
      this.canReceiveNotifications.next(false);
      console.warn('Service Worker aren\'t supported in this browser');
    }

    // We need the service worker registration to check for a subscription  
    navigator.serviceWorker.ready.then((serviceWorkerRegistration) => {
      // Do we already have a push message subscription?  
      console.log('Service Worker Ready to Roll');
      serviceWorkerRegistration.pushManager.getSubscription()
        .then((subscription) => {
          // Enable any UI which subscribes / unsubscribes from  
          // push messages.  
          this.canReceiveNotifications.next(true);
          //var pushButton = document.querySelector('.js-push-button');
          console.log('subscription', subscription);
          if (!subscription) {
            // We aren't subscribed to push, so set UI  
            // to allow the user to enable push  
            this.isSubscribed.next(false);
            return;
          } else {
            this.isSubscribed.next(true);
          }


          // Keep your server in sync with the latest subscriptionId
          //sendSubscriptionToServer(subscription);
          console.log('subscription', subscription);
          // Set your UI to show they have subscribed for  
          // push messages  
        })
        .catch(function (err) {
          console.warn('Error during getSubscription()', err);
        });
    });
  }

  initializeNotifications(registration: ServiceWorkerRegistration) {
    console.log('hello world');
    // Are Notifications supported in the service worker? 
    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
      //if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
      this.isNotificationSupported.next(false);
      console.warn('Notifications aren\'t supported.');
      this.canReceiveNotifications.next(false);
      return;
    } else {
      this.isNotificationSupported.next(true);
    }

    // Check the current Notification permission.  
    // If its denied, it's a permanent block until the  
    // user changes the permission  

    this.notificationPermission.next(Notification.permission);
    console.log(Notification);
    if (Notification.permission === 'denied') {
      console.warn('The user has blocked notifications.');
      this.canReceiveNotifications.next(false);
      return;
    }


    // Check if push messaging is supported  
    if (!('PushManager' in window)) {
      console.warn('Push messaging isn\'t supported.');
      this.isPushMessagingSupported.next(false);
      this.canReceiveNotifications.next(false);
      return;
    } else {
      this.isPushMessagingSupported.next(true);
    }


    this.notificationSubscribe();

  }

  subscribeChromeNotification() {

    chrome.runtime.onStartup.addListener(function () {
      chrome.storage.local.get("registered", function (result) {
        // If already registered, bail out.
        if (result["registered"])
          return;

        // Up to 100 senders are allowed.
        var senderIds = ["142303106012"];
        chrome.gcm.register(senderIds, (registrationId) => {
          if (chrome.runtime.lastError) {
            // When the registration fails, handle the error and retry the
            // registration later.
            return;
          }
          console.log('REGISTRATION ID:', registrationId);
          if (registrationId) {
            chrome.storage.local.set({ registered: true });
          }
        });
      });
    });
  }

  parseEndpoint(endpoint: string): string {
    if (endpoint.startsWith('http')) {
      let splitted = endpoint.split('/');
      endpoint = splitted[splitted.length - 1];
    }
    return endpoint;
  }

  /**
   * Send the user id and the jwt to the sw
   */
  sendTokenToSW() {
    if (this.swRegistration) {
      this.swRegistration.active.postMessage(
        JSON.stringify({ uid: this.authService.getId(), token: this.authService.getJwt() })
      )
    }

  }

  notificationSubscribe() {
    this.swRegistration.pushManager.subscribe({ userVisibleOnly: true })
      .then(sub => {
        console.log('endpoint:', sub.endpoint);


        this.sendTokenToSW();

        this.deviceService.find(this.parseEndpoint(sub.endpoint)).subscribe(
          device => {
            console.log('device kind of found', device);

            if (device.length <= 0) {
              this.deviceService.create(this.parseEndpoint(sub.endpoint)).subscribe(
                device => {

                },
                error => {
                  console.error('error saving device endpoint', error);
                }
              )
            } else {
              device.map(d => {
                if (!('user' in d) || !d.user) {
                  if (!d.id) {
                    console.error("This device doesn't have an id", device);
                  }
                  this.deviceService.addUser(d.id).subscribe(
                    dv => {
                      console.log('user added to device');
                    },
                    error => {
                      console.error('error putting user to the device', error);
                    }
                  )
                }
              })
            }
          }, error => {
            console.error('errorfindign out if the endpoint is already saved', error);
          }
        )

      }).catch(error => {
        console.log('error in subscription', error);
      })
  }

}
