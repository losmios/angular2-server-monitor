import { Injectable, NgZone } from "@angular/core";
import { Subject, Observable } from "rxjs/Rx";


declare let io: any;

if (io && io.sails) {


  if (io && io.socket && io.socket.isConnected()) {
    io.socket.disconnect();
  }

}

interface IJWRes {
  body: any;
  error?: any;
  headers: any;
  statusCode: number
}

@Injectable()
export class SailsService {

  private _io: any;
  private _opts: any
  private _restPrefix: string = "";
  private _serverUrl: string;
  private _pubsubSubscriptions: any;


  constructor(private zone: NgZone) {

    this._pubsubSubscriptions = {};

    this._opts = {
      url: null
    };


  }

  isConnected(): boolean {
    return this._io && this._io.isConnected();

  }

  get restPrefix(): string {
    return this._restPrefix;
  }

  set restPrefix(value: string) {
    if (value.length > 0) {
      if (value.charAt((value.length - 1)) == "/") {
        value = value.substr(0, value.length - 1);
      }
      this._restPrefix = value;
    }
  }


  get serverUrl(): string {
    return this._serverUrl;
  }

  set serverUrl(value: string) {
    if (value.length > 0) {
      if (value.charAt((value.length - 1)) == "/") {
        value = value.substr(0, value.length - 1);
      }
      this._serverUrl = value;
    }
  }

  public connect(url, opts?): Observable<any> {
    let subject = new Subject();
    if (this.isConnected()) {
      try {
        this._io.disconnect();
      } catch (error) {
        console.error("Error disconecting the socket, maybe it wasn't connected ", error);
      }

    }

    // Make URL optional
    if ('object' === typeof url) {
      this._opts = url;
      url = null;
    } else {
      if (opts) {
        this._opts = opts;
      }
      this._opts.url = url;
    }

    this._serverUrl = this._opts.url;

    this._io = io.sails.connect(this._opts);
    this._io.on('connect', () => {
      subject.next(true);
      subject.complete();
    });
    return subject.asObservable();
  }


  /**
   * @title request
   *
   * @description Send a virtual request to a Sails server using Socket.io.
   * This function is very similar to .get(), .post(), etc.
   * except that it provides lower-level access to the request headers, parameters,
   * method, and URL of the request.
   *
   * example:
   * @Component()
   * export class MyClass implements OnInit {
   *  constructor(private _sailsService:SailsService){}
   *
   *  ngOnInit{
   *
   *    let options = {
   *      method: 'get',
   *      url: 'http://localhost:1337/users'
   *      data: {},
   *      headers: {
   *        'x-csrf-token': 'ji4brixbiub3'
   *      }
   *    }
   *
   *    this._sailsService.request().subscribe(data => {
   *      // do something with the data
   *    })
   *
   *  }
   * }
   *
   * @param options
   * @return {Observable<T>}
     */
  request(options: any): Observable<any> {
    let subject = new Subject();

    this.zone.runOutsideAngular(() => {

      this._io.request(options, (resData, jwres: IJWRes) => {

        if (io.sails.environment != "production") {
          console.log("request::data", resData)
          console.log("request:jwr", jwres)
        }

        if (jwres.statusCode < 200 || jwres.statusCode >= 400) {
          subject.error(jwres.error)
        } else {
          this.zone.run(() => subject.next(resData));
        }

        subject.complete();


      })

    })

    return subject.asObservable();
  }

  /**
   *
   * @param url
   * @param data
   * @return {Observable<T>}
     */
  get(url, data?: any): Observable<any> {

    let subject = new Subject();
    let newUrl = this._restPrefix + url;
    this._io.get(newUrl, data, (resData, jwres: IJWRes) => {

      if (io.sails.environment != "production") {
        console.log("get::data", resData)
        console.log("get:jwr", jwres)
      }
      if (jwres.statusCode < 200 || jwres.statusCode >= 400) {
        console.error(`Error on get Method: ${newUrl}`, jwres);
        console.error('data: ', resData);
        let error = {
          jwres: jwres,
          data: resData,
          url: newUrl,
          previousData: data
        }
        subject.error(error);
      } else {
        subject.next(resData);
        //this.zone.run(() => subject.next(resData));
      }

      subject.complete();
    })

    return subject.asObservable();

  }

  /**
   *
   * @param url
   * @param data
   * @return {Observable<T>}
     */
  post(url, data?: any): Observable<any> {

    let subject = new Subject();

    this.zone.runOutsideAngular(() => {

      this._io.post(url, data, (resData, jwres: IJWRes) => {
        if (io.sails.environment != "production") {
          console.log("post::data", resData);
          console.log("post:jwr", jwres);
        }

        if (jwres.statusCode < 200 || jwres.statusCode >= 400) {
          let error = {
            jwres: jwres,
            data: resData,
            url: url,
            previousData: data
          }
          subject.error(error);
        } else {
          this.zone.run(() => subject.next(resData));

        }

        subject.complete();
      })

    });
    return subject.asObservable();
  }

  /**
   *
   * @param url
   * @param data
   * @return {Observable<T>}
     */
  put(url, data?: any): Observable<any> {

    let subject = new Subject();
    this._io.put(url, data, (resData, jwres: IJWRes) => {
      if (io.sails.environment != "production") {
        console.log("put::data", resData);
        console.log("put:jwr", jwres);
      }

      if (jwres.statusCode < 200 || jwres.statusCode >= 400) {
        let error = {
          jwres: jwres,
          data: resData,
          url: url,
          previousData: data
        }
        subject.error(error);
      } else {
        subject.next(resData);
        //this.zone.run(() => subject.next(resData));
      }

      subject.complete();
    })
    return subject.asObservable();
  }

  /**
   *
   * @param url
   * @param data
   * @return {Observable<T>}
     */
  delete(url, data?: any): Observable<any> {

    let subject = new Subject();

    this.zone.runOutsideAngular(() => {


      this._io.delete(url, data, (resData, jwres: IJWRes) => {

        if (io.sails.environment != "production") {
          console.log("delete::data", resData);
          console.log("delete:jwr", jwres);
        }

        if (jwres.statusCode < 200 || jwres.statusCode >= 400) {
          let error = {
            jwres: jwres,
            data: resData,
            url: url,
            previousData: data
          }
          subject.error(error);
        } else {
          //subject.next(resData);
          this.zone.run(() => subject.next(resData));
        }

        subject.complete();
      })

    });
    return subject.asObservable();
  }

  /**
   *
   * @param eventIdentity
   * @return {Observable<T>}
     */
  on(eventIdentity: string): Observable<any> {

    if (!this._pubsubSubscriptions[eventIdentity] || this._pubsubSubscriptions[eventIdentity].isComplete) {
      this._pubsubSubscriptions[eventIdentity] = new Subject();

      this.zone.runOutsideAngular(() => {

        this._io.on(eventIdentity, msg => {

          if (io.sails.environment != "production") {
            console.log(`on::${eventIdentity}`, msg);
          }

          this.zone.run(() => {
            this._pubsubSubscriptions[eventIdentity].next(msg);
          })

        })

      })
    }

    return this._pubsubSubscriptions[eventIdentity].asObservable();
  }

  // public off(eventIdentity:string) {
  //
  //   if(<Subject>this._pubsubSubscriptions[eventIdentity]) {
  //
  //   }
  //
  //   if (!<Subject>this._pubsubSubscriptions[eventIdentity].isComplete) {
  //     <Subject>this._pubsubSubscriptions[eventIdentity].complete();
  //   }
  //
  //
  // }

}
