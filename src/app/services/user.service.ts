import { Injectable } from '@angular/core';
//import { AuthService } from '../auth.service';
import { SailsService } from './sails.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { User } from '../server-monitor.models';


@Injectable()
export class UserService {

	constructor(private sailsService: SailsService) { 
		//TODO make an implementation in which this doesnt have to connect
		this.sailsService.connect('http://localhost:1337');
	}

	emailExists(email : string): Observable<boolean>{
		let subject = new Subject<boolean>();
		this.sailsService.get('/auth?email='+email).subscribe(
			res => {
				if(res.length){
					subject.next(true)
				}else{
					subject.next(false)
				}
			},error => {
				console.error('Connection error checking if the email exists')
				subject.next(false)
			}
		)
		return subject.asObservable();
	}

	create(user : User){
		let subject = new Subject<any>();
		if(
			! (user.hasOwnProperty('lastName') &&
			user.hasOwnProperty('email') &&
			user.hasOwnProperty('password'))
			){
				setTimeout(subject.error('user object invalid'), 0);
				return subject.asObservable();
		}
		this.emailExists(user.email).subscribe(
			exists => {
				if(exists){
					subject.error('Email already registered')
					subject.complete();
				}
			}
		)
		this.sailsService.post('/auth/login',{email: user.email, password: user.password})
			.subscribe(res => {
				
			})
	}
}