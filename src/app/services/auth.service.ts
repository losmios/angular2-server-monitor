import { Injectable } from '@angular/core';
import { SailsService } from './sails.service';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { SERVER_URL } from '../server-url';
import { User } from '../server-monitor.models';


@Injectable()
export class AuthService {

  private isAuthenticated: Subject<boolean> = new BehaviorSubject<boolean>(false);

  private jwt: string;
  private expires: number = 0;
  error: any;
  email: string = '';
  private id: number;
  redirectUrl: string = '/dashboard';
  isConnected: boolean;

  private user: Subject<any> = new BehaviorSubject<any>(null);
  private jwt$: Subject<string> = new BehaviorSubject<string>(this.jwt);

  /**
   * Makes a dummy requests to some url that requires a jwt access token,
   * if the token is valid the user info will be filled
   * @requires to have jwt and userId binded to the this
   * @returns true if the request code is 200 and false for 403 forbidden
   */
  validateJwt() {
    let subject = new Subject<boolean>();
    if (!this.jwt || !this.id) {
      console.debug('this.jwt and this.id not found in the authService, validateJwt will return false')
      setTimeout(() => {
        subject.next(false)
        subject.complete();
      });
    } else {
      this.sailsService.get('/user/' + this.id + '?access_token=' + this.jwt).subscribe(
        user => {
          this.getUserInfo();
          subject.next(true)
          this.jwt$.next(this.jwt);
          subject.complete()
        }, error => {
          console.log('JWT not valid');
          subject.next(false)
          this.jwt$.next(null);
          subject.complete()
        }
      )
    }

    return subject.asObservable();
  }

  getJwtObservable() {
    return this.jwt$.asObservable();
  }

  constructor(private sailsService: SailsService) {
    let opts = {};
    this.jwt = localStorage.getItem('jwt');
    this.id = parseInt(localStorage.getItem('userId'));
    //connect first before validating token
    this.sailsService.connect(SERVER_URL).subscribe(
      isConnected => {
        this.validateJwt().subscribe(
          valid => {
            if (valid) {

              opts = {
                url: SERVER_URL,
                headers: {
                  access_token: this.jwt
                }
              }
              this.sailsService.connect(opts).subscribe(
                isConnected => {
                  this.isAuthenticated.next(true);
                }
              )

            } else {
              this.isAuthenticated.next(false);
              opts = {
                url: SERVER_URL
              }
            }
          }
        );
      }
    );



    //get the user data when is authenticated
    this.isAuth().subscribe(isAuth => {
      console.debug('is auth observable triggered');
      if (isAuth) {
        this.getUserInfo();
      }
    })
  }

  getJwt() {
    return this.jwt;
  }

  getId() {
    return this.id
  }

  getUser() {
    return this.user.asObservable();
  }

  private parseUserRequest(user) {
    let aux = {
      email: user.auth.email,
      id: user.id,
      blocked: user.auth.blocked
    }
    aux['firstName'] = user.firstName || "";
    aux['lastName'] = user.lastName || "";
    return aux;
  }

  private getUserInfo() {
    this.sailsService.get('/user/' + this.id + '?populate=[auth]').subscribe(
      user => {
        let aux = this.parseUserRequest(user);
        this.user.next(aux);
      },
      error => {
        console.error('Error getting the user info', error);
      }
    )
  }

  update(user: {
    firstName: string,
    lastName: string
  }) {
    return this.sailsService.put('/user/' + this.getId(), user).do(user => {
      this.user.next(this.parseUserRequest(user));
    })
  }

  private _login(email: string, password: string) {
    let subject = new Subject<any>();
    let user = {
      email: email,
      password: password
    }
    if (!this.sailsService.isConnected()) {
      subject.error('No internet connection');
    } else {
      let observable = this.sailsService.post('/auth/login', user);
      observable
        .subscribe(
        resData => {
          this.id = resData.id
          localStorage.setItem('userId', '' + this.id);
          this.sailsService.get('/user/jwt').subscribe(
            data => {
              //Save this in localstorage
              this.jwt = data.token;
              this.jwt$.next(this.jwt);
              let options = {
                url: SERVER_URL,
                headers: {
                  access_token: this.jwt
                }
              }
              this.sailsService.connect(options).subscribe(
                isConnected => {
                  this.isAuthenticated.next(true);
                  //this.getUserInfo();
                  subject.next(resData);
                  console.log('token accessed', this.jwt);
                  localStorage.setItem('jwt', this.jwt);
                  this.expires = data.expires;
                }
              )

            },
            error => {
              console.error('error getting the jwt', error);
              this.user.next(null);
            },
            () => {
              console.log('login completed');
            }
          )
        },
        error => {
          this.isAuthenticated.next(false);
          this.user.next(null);
          this.error = error;
          subject.error(error);
          subject.complete();
        }
        )
    }
    return subject.asObservable();

  }

  login(email: string, password: string): Observable<boolean> {
    let subject = new Subject<boolean>()
    if (!this.sailsService.isConnected()) {
      subject.error({
        type: 'connection',
        message: 'No interet connection'
      });
      subject.complete();
    } else {


      this.emailExists(email)
        .subscribe(
        exists => {
          if (exists) {
            this._login(email, password)
              .subscribe(
              res => {
                subject.next(true);
                subject.complete();
              },
              error => {
                subject.next(false);
                subject.complete();
              }
              )
          } else {
            subject.next(false);
            subject.complete()
          }
        })
    }
    return subject.asObservable();
  }

  isAuth() {
    return this.isAuthenticated.asObservable();
  }

  logout() {
    let subject: Subject<boolean> = new Subject<boolean>();
    this.sailsService.get('/auth/logout')
      .subscribe(
      resData => {
        this.isAuthenticated.next(false);
        localStorage.setItem('jwt', '');
        localStorage.setItem('userId', '');
        subject.next(true);
      }, error => {
        console.error(error);
        subject.error(error);
      })
    return subject.asObservable();
  }

  emailExists(email: string): Observable<boolean> {
    let subject = new Subject<boolean>();
    this.sailsService.get('/auth?email=' + email).subscribe(
      res => {
        if (res.length) {
          subject.next(true)
        } else {
          subject.next(false)
        }
      }, error => {
        console.error('Connection error checking if the email exists')
        subject.next(false)
      }
    )
    return subject.asObservable();
  }

  register(user: User) {
    let subject = new Subject<any>();
    this.emailExists(user.email).subscribe(
      exists => {
        if (exists) {
          subject.error('Email already exists')
          subject.complete()
        } else {
          this._login(user.email, user.password)
            .subscribe(
            res => {
              let names = {
                firstName: user.firstName,
                lastName: user.lastName
              }
              subject.next(res);
              this.sailsService.put('/user/' + res.id, names)
                .subscribe(
                user => {

                  this.user.next(this.parseUserRequest(user));
                  //this.getUserInfo();
                  subject.complete()
                }, error => {
                  console.error('error putting names to user');
                  subject.error(error);
                  subject.complete();
                }
                )
            }, error => {
              console.error('error authenticating the user', error);
              subject.error(error);
              subject.complete();
            }
            )
        }
      }
    )

    return subject;
  }


}
