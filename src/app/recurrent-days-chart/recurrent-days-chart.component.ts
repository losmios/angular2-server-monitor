import { Component, OnInit, Input } from '@angular/core';
import { Incident } from '../server-monitor.models';
import * as moment from 'moment';

@Component({
  selector: 'app-recurrent-days-chart',
  templateUrl: './recurrent-days-chart.component.html',
  styleUrls: ['./recurrent-days-chart.component.scss']
})
export class RecurrentDaysChartComponent implements OnInit {

  @Input()
  incidents: Incident[];

  chart: any;

  labels: string[];
  data: any;
  chartType = 'radar';
  chartOptions = {};

  constructor() { }

  ngOnInit() {
    this.labels = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes"];
    this.data = [{
      label: 'Cantidad de Incidencias',
      data: [1, 0, 0, 0, 0, 0, 0]
    }];
    this.loadChartData(this.incidents);
  }

  loadChartData(incidents: Incident[]) {


  }

  getIncidentCounter(incidents: Incident[]) {
    let counter = [];
    for (let i = 0; i < 7; i++) {
      let count = incidents.filter((inc: Incident) => moment(inc.createdAt).day() === i);
      if (count) {
        counter.push(count.length);
      } else {
        counter.push(0);
      }
    }
    return counter;
  }

}
