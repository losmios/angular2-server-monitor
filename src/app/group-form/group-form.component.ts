import { Component, OnInit, EventEmitter } from '@angular/core';
import { ThingService } from '../services/thing.service';
import { FormControl } from '@angular/forms';
import { Group } from '../server-monitor.models';
import { MaterializeAction } from 'angular2-materialize';

declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'group-form',
	templateUrl: 'group-form.component.html',
	styleUrls: ['./group-form.component.scss']
})
export class GroupFormComponent implements OnInit {
	constructor(private thingService: ThingService) { }

	group_name = new FormControl();
	valid: boolean;
	error_msg: string;
	success: boolean;
	success_msg: string;
	error: boolean;
	joining: boolean = false;
	dirty: boolean;

	groups: Array<Group> = [];
	displayGroups: Array<Group>
	joinedGroups: Array<Group>
	selectedGroup: Group = {
		name: '',
		id: 0
	};

	modalActions = new EventEmitter<string | MaterializeAction>();

	private validateGroupName(name: string): boolean {
		if (!name) return false;
		if (name.length < 4) {
			return false;
		} else {
			return true;
		}
	}

	ngOnInit() {
		this.valid = true;
		this.success = false;
		this.dirty = false;
		this.group_name.valueChanges
			.debounceTime(400)
			.subscribe(name => {
				if (!this.validateGroupName(name)) {
					this.valid = false;
					console.log('debe tener mas de 4 caracteres')
					this.error_msg = "Debe tener mas de 4 caracteres"
					return false;
				}
				this.error_msg = null;
				this.thingService.findOneGroup(name)
					.subscribe(groups => {
						console.log('great', groups);
						if (groups.length) {
							this.valid = false;
							this.error_msg = "Ya existe un grupo con ese nombre, por favor escoja otro"
						} else {
							this.valid = true;
						}
					})


			})



		this.group_name.valueChanges.subscribe(
			name => {
				if (!this.dirty) {
					this.dirty = true;
				}
				if (!this.groups || !this.groups.length) {
					return;
				}
				var re = new RegExp(name, "i")
				if (name !== '') {
					this.displayGroups = this.groups.filter(g => re.test(g.name));
				} else {
					this.displayGroups = this.groups;
				}
			}
		)
		this.thingService.getPublicGroups().subscribe(
			groups => {
				this.thingService.getJoinedGroups().subscribe(
					joinedGroups => {
						groups = groups || [];
						this.groups = groups.filter(g => !joinedGroups.find(jg => jg.id === g.id))
						this.displayGroups = this.groups;
					},
					error => {
						console.error('Error getting the joined groups', error);
					}
				)
			}
		)

		document.querySelector('a').addEventListener('click', (ev) => {
			ev.preventDefault();
		})
	}

	selectGroup(group) {
		this.selectedGroup = group;
		this.modalActions.emit({ action: 'modal', params: ['open'] })
	}

	reset() {
		this.group_name.reset();
		this.valid = true;
		this.displayGroups = this.groups;
		this.error = false;
		this.dirty = false;

	}

	submit() {
		if (this.valid) {
			console.log('creando grupo', this.group_name.value);
			this.thingService.createGroup(this.group_name.value)
				.subscribe(group => {
					//this.success_msg = "Grupo Creado";
					this.groups = this.groups.filter(g => g.id !== group.id);
					this.success = true;
					Materialize.toast('Grupo Creado', 2000);
					this.reset();
				}, error => {
					this.error = true;
					this.error_msg = "Error de conexion";
				})
		} else {
			this.error_msg = "Nombre de grupo invalido, no se puede crear, por favor escoja otro nombre";
		}
	}

	closeModal() {
		this.modalActions.emit({ action: 'modal', params: ['close'] })
	}

	joinGroup() {
		this.joining = true;
		this.thingService.joinGroup(this.selectedGroup.id).subscribe(
			res => {
				this.joining = false;
				this.groups = this.groups.filter(g => g.id !== this.selectedGroup.id)
				this.displayGroups = this.groups;
				Materialize.toast('Se ha unido al grupo ' + this.selectedGroup.name, 2000);
				this.selectedGroup = {
					id: null,
					name: ''
				}
				this.closeModal();
			},
			error => {
				console.error(error);
			}
		)
	}

}