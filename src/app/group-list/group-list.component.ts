import { Component, OnInit } from '@angular/core';
import { ThingService } from '../services/thing.service';
import { Group } from '../server-monitor.models';

@Component({
  selector: 'group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss']
})
export class GroupListComponent implements OnInit {

  groups: Group[];

  constructor(private thingService: ThingService) { }

  ngOnInit() {
    this.thingService.getGroups()
      .subscribe(
      groups => {
        this.groups = groups;
        this.groups.map(g => {
          g.url = "/group/" + g.id;
        })
      }
      )
  }

}
