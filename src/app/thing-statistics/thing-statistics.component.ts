import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ThingService } from '../services/thing.service';
import { Thing, Incident } from '../server-monitor.models';
import * as numeral from 'numeral';
import * as moment from 'moment';

@Component({
  selector: 'app-thing-statistics',
  templateUrl: './thing-statistics.component.html',
  styleUrls: ['./thing-statistics.component.scss']
})
export class ThingStatisticsComponent implements OnInit {

  thing: Thing;
  time: number;
  maintenanceTime: string;
  incidentsNumber: number;
  private threeMonths: number = 7776000; //3 months in seconds


  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public daysLabels: string[] = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
  public hoursLabels = []
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Incidencias' }
  ];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  constructor(
    private route: ActivatedRoute,
    private thingService: ThingService
  ) { }

  ngOnInit() {
    for (let i = 0; i < 24; i++) {
      this.hoursLabels.push(i + 'h');
    }
    this.route.params.subscribe(
      (params: Params) => {
        let thingId = +params['thingId'];
        if (!thingId) {
          console.error('Thing id not provided in the route, checkout the params', params);
        }
        this.thingService.getThings().subscribe(
          things => {
            let newThing = things.find(thing => thing.id === thingId);
            let onlineTime = numeral(parseInt(newThing.onlineTime, 10)).format('00:00:00');
            this.maintenanceTime = numeral(this.threeMonths - parseInt(newThing.onlineTime, 10)).format('00:00:00');
            this.thing = Object.assign({}, newThing, {
              onlineTime: onlineTime
            });
          }
        )
        this.thingService.getIncidents({
          thing: this.thing.id,
          type: 'fail',
          populate: []
        }).subscribe(incidents => {
          this.incidentsNumber = incidents.length;
          this.thing.sensors = this.thing.sensors.map(sensor => {
            let sensorIncidents = incidents.filter(i => i.sensor === sensor.id);
            let counter = []
            for (let i = 0; i < 7; i++) {
              let aux = sensorIncidents.filter(incs => moment(incs.createdAt).day() === i);
              counter.push(aux.length);
            }
            sensor['recurrentDays'] = [{
              data: counter,
              label: 'Incidencias'
            }];

            sensor['recurrentHours'] = [{
              data: this.getRecurrentHours(incidents),
              label: 'Incidencias'
            }]

            return sensor;
          });
        }, error => {
          console.error('Error getting the incidents of thing, ', this.thing);
        })
      }
    )
  }

  getRecurrentHours(incidents: Incident[]) {
    let counter = [];
    for (let i = 0; i < 24; i++) {
      let aux = incidents.filter(inc =>
        moment(inc.createdAt).hour() === i
      );
      counter.push(aux.length);
    }
    return counter;
  }

  numeralFormat(num) {
    return numeral(num).format('00:00:00');
  }

}
