import { Component, OnInit, Input } from '@angular/core';
import { ThingService } from '../services/thing.service';
import { AuthService } from '../services/auth.service';

import { Group, User } from '../server-monitor.models';
import { Observable } from 'rxjs';


interface Member extends User {
  email: string;
  permissions: Array<string>;
  status: string;
}

@Component({
  selector: 'app-group-user-list',
  templateUrl: './group-user-list.component.html',
  styleUrls: ['./group-user-list.component.scss']
})
export class GroupUserListComponent implements OnInit {

  @Input() group: Observable<Group>;
  members: any
  userMember: any
  groupId: number;

  joining: Array<any>

  constructor(
    private thingService: ThingService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.group.subscribe(
      g => {
        this.loadMembers(g.id);
        this.groupId = g.id;
      }
    )

  }

  unMember(event: Event, member: any) {
    event.target['classList'].add('disabled');
    let permissions = ['blocked']
    this.thingService.removeFromGroup(this.groupId, member.id)
      .subscribe(sucess => {
        this.members = this.members.filter(m => m.id !== member.id);
      }, error => {
        console.error('Error deleting a member', error);
      })
  }


  loadMembers(id: number) {
    this.thingService.getGroupMembers(id).subscribe(
      members => {
        console.log('Members here: ', members);

        this.members = members.filter(m => m.status === 'active');
        this.joining = members.filter(m => m.status === 'joining');
        this.userMember = this.members.find(m => m.id === this.authService.getId());
      },
      error => {
        console.error('Error getting the members of group (${this.group.name})', error);
      }
    )
  }

  can(member, permission: string) {
    if (!member.permissions) {
      return false;
    }
    return (member.permissions.indexOf(permission) !== -1)
  }

  addMember(member: Member) {

    this.thingService.updateMember(this.groupId, member.id, 'active', ['read'])
      .subscribe(
      gp => {
        this.members.push(member);
        this.joining = this.joining.filter(m => m.id !== member.id);
      }, error => {
        console.error('Error accepting user joining', error);
      }
      )
  }

  adminCheckbox(event: Event, member) {

    if (event.target['checked']) {
      if (!this.can(member, 'admin')) {
        member.permissions.push('admin');
      } else {
        return;
      }
    } else {
      if (this.can(member, 'admin')) {
        member.permissions = member.permissions.filter(p => p !== 'admin');
      } else {
        return;
      }
    }
    event.target['disabled'] = true;
    this.thingService.memberPutPermissions(this.groupId, member.id, member.permissions).subscribe(
      gp => {
        event.target['disabled'] = false;
      }, error => {
        event.target['disabled'] = false;
        event.target['checked'] = !event.target['checked'];
        console.error('Error putting new permissions for group ${this.groupId} and user ${member.id}', error);
      }
    )
  }

}
