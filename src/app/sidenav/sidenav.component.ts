import { 
  Component, 
  OnInit, 
  Input
} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Group } from '../server-monitor.models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {

  isAuth: boolean = false;
  configState = 'hidden';
  user: any;

  constructor(
  private authService: AuthService,
  private router: Router
  ) { 
   }

  ngOnInit() {
    this.authService.isAuth()
      .subscribe(
        isAuth => {
          this.isAuth = isAuth;
        }
      )
    this.authService.getUser().subscribe(
      user => {
        if(user){
          this.user = user;
        }
      }
    )

  }

  toogle(){
    if(this.configState === 'hidden'){
      this.configState = 'shown'
    }else{
      this.configState = 'hidden';
    }
  }

  goToProfile(){
    this.router.navigate(['/profile']);
  }


  logout(){
    this.authService.logout().subscribe(
      loggedOut => {
        this.router.navigate(['/']);
      }
    )
  }

}
