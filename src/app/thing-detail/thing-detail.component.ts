import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';

import { Sensor } from '../server-monitor.models';

@Component({
  selector: 'app-thing-detail',
  templateUrl: './thing-detail.component.html',
  styleUrls: ['./thing-detail.component.scss']
})
export class ThingDetailComponent implements OnInit, OnChanges {

  @Input()
  thing: any;

  @Input()
  canEdit: boolean


  editState = 'hidden';

  sensorsTypes: Array<Array<Sensor>>


  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    console.debug(`Thing#${this.thing.id} changed`, changes);
  }

  ngOnInit() {
    /*
    this.thing.sensors.subscribe(sensor => {
      console.log('sensor change', sensor);
    })
    */
    this.sensorsTypes = [];
    let types = {};
    this.thing.sensors.map(sensor => {
      if (!((sensor.type + "") in types)) {
        types[sensor.type] = [];
      }
      types[sensor.type].push(sensor);
    })
    for (let p in types) {
      if (types.hasOwnProperty(p)) {
        this.sensorsTypes.push(types[p]);
      }
    }
    console.debug('Hey these are the sensors', this.thing.sensors);

  }

  alert() {
    return this.thing.alert;
  }

  quickConfig() {
    if (this.editState === 'hidden') {
      this.editState = 'shown';
    } else {
      this.editState = 'hidden';
    }
  }


}
