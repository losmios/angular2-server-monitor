import {
  Component,
  OnInit,
  Input,
  trigger,
  state,
  style,
  transition,
  animate,
  Output,
  EventEmitter
} from '@angular/core';

import { Sensor } from '../server-monitor.models';
import { ThingService } from "../services/thing.service";

@Component({
  selector: 'app-ram-collapsible',
  templateUrl: './ram-collapsible.component.html',
  styleUrls: ['./ram-collapsible.component.scss'],
  animations: [
    trigger('listState', [
      state('hidden', style({
        height: 0
      })),
      state('shown', style({
        height: '*'
      })),
      transition('hidden <=> shown', animate('500ms ease-out'))
    ])
  ]
})
export class RamCollapsibleComponent implements OnInit {

  @Input()
  sensors: Sensor[] = [];


  type: string;
  unit: string;
  listState: string = 'hidden';
  totalMem: number

  constructor(
    private thingService: ThingService
  ) { }

  ngOnInit() {
    this.type = this.sensors[0].type;
    this.unit = '%'
    this.sensors.map(sensor => {
      if (sensor.name.toLowerCase() === 'total') {
        this.totalMem = sensor.lastMeasure.value;
      }
    })
    this.sensors = this.sensors.filter(s => s.name.toLowerCase() !== 'total');
    for (let i = 0; i < this.sensors.length; i++) {
      let unitId = null;
      if (!('abbreviation' in this.sensors[i].lastMeasure.unit)) {
        unitId = this.sensors[i].lastMeasure.unit;
        this.sensors[i]['unit'] = '';
      } else {
        this.sensors[i]['unit'] = this.sensors[i].lastMeasure.unit.abbreviation;
      }

      this.sensors[i]['min'] = 0;
      if (this.sensors[i].name !== 'percent') {
        this.sensors[i]['max'] = this.totalMem;
      }
      if (this.sensors[i].name.toLowerCase() === 'percent') {
        this.sensors[i].name = "Porcentaje";
      }
      if (this.sensors[i].name.toLowerCase() === 'available') {
        this.sensors[i].name = "Disponible";
      }
      if (this.sensors[i].name.toLowerCase() === 'used') {
        this.sensors[i].name = "Utilizada";
      }
      if (unitId) {
        this.thingService.getUnit(unitId).subscribe(unit => {
          console.debug('Getting the unit', unit);
          this.sensors[i].unit = unit.abbreviation;
        }, error => {
          console.error('error getting the unit', error);
        })
      }

    }
    var ids = this.sensors.map(s => s.id);

    this.thingService.getSensorObservable()
      .subscribe(sensor => {
        if (!(sensor => sensor.id in this.sensors.map(s => s.id))) {
          return;
        }
        this.sensors = this.sensors.map(s => {
          if (s.id === sensor.id) {
            return Object.assign(sensor, s);
          }
          return s;
        })
      }, error => {
        console.error('Error getting the sensor sockets', error);
      })
  }

  percentage() {
    let sensor = this.sensors.find(s => s.name.toLowerCase() === 'porcentaje');
    return sensor.lastMeasure.value;
  }

  toggleList() {
    if (this.listState === 'hidden') {
      this.listState = 'shown';
    } else {
      this.listState = 'hidden';
    }
  }

}
