import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-progress-indicator',
  templateUrl: './progress-indicator.component.html',
  styleUrls: ['./progress-indicator.component.scss']
})
export class ProgressIndicatorComponent implements OnInit {

  @Input()
  name: string = '';

  @Input()
  value: number = 0;

  @Input()
  unit: string = '';

  @Input()
  color: string = '';

  @Input()
  icon: string = '';

  @Input()
  min: number;

  @Input()
  max: number;

  @Input()
  size: string = '';

  @Input()
  alert: boolean;

  height: string = '4em';

  debugMsg: string;

  constructor() { }

  ngOnInit() {
    this.min = this.min || 0;
    this.max = this.max || 100;
    if(!this.color){
      this.color = 'indigo';
    }
    if(!this.size){
      this.size = 'normal';
    }
  }

  calculatedValue(){
    let value = ((this.value - this.min) * 100 ) / (this.max - this.min)
    return Math.round(value * 100) / 100;
  }

  widthPercentage(){
    return this.calculatedValue()+"%";
  }

  getSize(){
    switch(this.size){
      case 'small':
        this.height = '2em';
        break;
      case 'normal':
        this.height = '4em'
        break;
      case 'big':
        this.height = '6em';
        break;
      default:
        this.height = '4em';
        break;
    }
  }
}
