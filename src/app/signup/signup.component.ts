import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { emailValidator, emailRepeatedValidator } from '../validators/email.validator';
import { AuthService } from '../services/auth.service';
import { sameAsValidator, onlyLetters } from '../validators/field.validator';
import { Router } from '@angular/router';

@Component({
	selector: 'signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

	//password = new FormControl()
	//repeat_password = new FormControl()
	repeat_password_valid: boolean
	valid: boolean
	error: boolean

	success: boolean = false;
	sendingForm: boolean = false;

	registerForm: FormGroup;

	constructor(
		private formBuilder: FormBuilder,
		private authService: AuthService,
		private router: Router
		) { }

	ngOnInit() {

		this.registerForm = this.formBuilder.group({
			firstName: ['', [
				Validators.required,
				Validators.minLength(2),
				Validators.maxLength(35),
				onlyLetters
			]
			],
			lastName: ['', [
				Validators.required,
				Validators.minLength(2),
				Validators.maxLength(35),
				onlyLetters
			]
			],
			email: ['', [Validators.required, emailValidator], emailRepeatedValidator(this.authService)],
			password: ['', [
				Validators.required,
				Validators.minLength(8),
				Validators.maxLength(100)
			]
			],

		})


		this.registerForm.addControl(
			'repeatPassword',
			new FormControl('', [
				Validators.required,
				sameAsValidator(this.registerForm.get('password'), 'Constraseña')])
		)

		this.valid = false;

	}

	logForm(user: {
		firstName: string
		lastName: string
		email: string
		password: string
		repeatPassword
	}) {
		if (!this.registerForm.valid || !this.registerForm.dirty) {
			return
		}
		delete user.repeatPassword;
		this.sendingForm = true;
		this.authService.register(user)
			.subscribe(
			user => {
				this.sendingForm = false;
				this.success = true;
				this.registerForm.reset();
				this.router.navigate(['/findgroup'])
			},
			error => {
				this.error = true;
				console.error(error);
				this.sendingForm = false;
			}
			)
	}

}