import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import * as moment from 'moment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-notification-center',
  templateUrl: './notification-center.component.html',
  styleUrls: ['./notification-center.component.scss']
})
export class NotificationCenterComponent implements OnInit {

  notifications: Array<any>;

  constructor(
    private notificationService: NotificationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.notificationService.getNotifications()
      .subscribe(notifications => {
        console.debug('Here are the user notifications', notifications);
        if (!notifications.length) {
          return;
        }
        this.notifications = notifications.map(notif => {
          delete notif.url;
          let name = `${moment(notif.createdAt).calendar()} | ${notif.title}`;
          return notif = Object.assign({}, notif, {
            name: name
          })
        })
      })
  }

  goToReports(notification) {
    if (!('group' in notification)) {
      this.notificationService.getNotification(notification.id).subscribe(
        notif => {
          this.goToReports(notif);
        }, error => {
          console.error('error geting the notification', error);
        }
      )
    } else {
      this.router.navigate([`/group/${notification.group}/reports`], { queryParams: { from: notification.createdAt } });
    }

  }

  clickHandle(notification) {
    console.log('notification clicked', notification);
    if (notification.read) {
      this.goToReports(notification);
      return;
    }
    this.notificationService.setAsRead(notification.id).subscribe(notif => {
      this.notifications = this.notifications.map(notif2 => {
        if (notif2.id === notif.id) {
          notif2['read'] = notif.read;
        }
        return notif2;
      })
      this.goToReports(notification);
    }, error => {
      console.error('Error setting the notification as read', error);
    })
  }

}
