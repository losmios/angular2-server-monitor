import { Component, OnInit, Input } from '@angular/core';
import { Sensor, Unit } from '../server-monitor.models';
import { ThingService } from '../services/thing.service';

@Component({
  selector: 'app-sensor-detail',
  templateUrl: './sensor-detail.component.html',
  styleUrls: ['./sensor-detail.component.css']
})
export class SensorDetailComponent implements OnInit {


  @Input()
  sensor: Sensor;

  percentage: string;

  value: number;

  @Input()
  color: string;

  unit: string;

  constructor(
    private _thingService: ThingService) { }

  ngOnInit() {
    if(!this.color){
      this.color = 'indigo lighten-2';
    }
    this.percentage = "0%";
    this._thingService.getSensor(this.sensor.id)
      .subscribe(
      sensor => {
        if(!sensor){
          console.error('Sensor not found');
        }
        this.sensor = sensor
        this.value = sensor.lastMeasure.value
        if(!this.value){
          this.value = 0;
        }
        this.percentage = this.value + "%";
        console.debug('im getting this unit from the sensor', this.sensor.lastMeasure.unit);
        this.unit = this.sensor.lastMeasure.unit.abbreviation || '%';
      }, error => {
        console.error('Error getting the sensor #' + this.sensor.id, error);
      }
      )
  }


}
