/*
Modules Imports
*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { HighlightJsModule, HighlightJsService } from 'angular2-highlight-js';

/**
 * Importing Providers
 */
import { ThingService } from './services/thing.service';
import { AuthService } from './services/auth.service';
import { SailsService } from './services/sails.service';
import { GroupService } from './services/group.service';
import { UserService } from './services/user.service';
import { ServiceWorkerService } from './services/service-worker.service';
import { DeviceService } from './services/device.service';
import { NotificationService } from './services/notification.service';
import { NavbarService } from './services/navbar.service';


/**
 * Importing Components
 */
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SensorDetailComponent } from './sensor-detail/sensor-detail.component';
import { IndexComponent } from './index/index.component';
import { SignupComponent } from './signup';
import { FieldComponent } from './forms-materialize/field.component';
import { DashboardComponent } from './dashboard';
import { IndicatorComponent } from './indicator';
import { HomeComponent } from './home-moises/home.component';
import { NavBarComponent } from './navbar';
import { ThingListComponent } from './thing-list/thing-list.component';
import { ThingDetailComponent } from './thing-detail/thing-detail.component';
import { GroupFormComponent } from './group-form/group-form.component';

/*
Routes Imports
*/
import { routing, appRoutingProviders } from './app.routing';
import { SidenavComponent } from './sidenav/sidenav.component';
import { GroupListComponent } from './group-list/group-list.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { SensorTypeCardComponent } from './sensor-type-card/sensor-type-card.component';
import { SensorTypeCollapsibleComponent } from './sensor-type-collapsible/sensor-type-collapsible.component';
import { ProgressIndicatorComponent } from './progress-indicator/progress-indicator.component';
import { ServiceWorkerComponent } from './service-worker/service-worker.component';
import { NotificationsCheckboxComponent } from './notifications-checkbox/notifications-checkbox.component';
import { NotificationCenterComponent } from './notification-center/notification-center.component';
import { CollectionComponent } from './collection/collection.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { UserModifyComponent } from './user-modify/user-modify.component';
import { GroupExitComponent } from './group-exit/group-exit.component';
import { NotificationSubscribeComponent } from './notification-subscribe/notification-subscribe.component';
import { GroupConfComponent } from './group-conf/group-conf.component';
import { GroupUserListComponent } from './group-user-list/group-user-list.component';
import { ThingQuickUpdateComponent } from './thing-quick-update/thing-quick-update.component';
import { GroupReportsComponent } from './group-reports/group-reports.component';
import { GroupNavbarComponent } from './group-navbar/group-navbar.component';
import { ThingReportsComponent } from './thing-reports/thing-reports.component';
import { DateRangePickerComponent } from './date-range-picker/date-range-picker.component';
import { DocsIndexComponent } from './docs/docs-index/docs-index.component';
import { NewThingComponent } from './docs/new-thing/new-thing.component';
import { RamCollapsibleComponent } from './ram-collapsible/ram-collapsible.component';
import { ReportsIndexComponent } from './reports-index/reports-index.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ThingStatisticsComponent } from './thing-statistics/thing-statistics.component';
import { ThingGeneralStatisticsComponent } from './thing-general-statistics/thing-general-statistics.component';
import { SimpleCardComponent } from './simple-card/simple-card.component';
import { IncidentChartComponent } from './incident-chart/incident-chart.component';
import { RecurrentDaysChartComponent } from './recurrent-days-chart/recurrent-days-chart.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SensorDetailComponent,
    ThingDetailComponent,
    ThingListComponent,
    GroupFormComponent,
    IndexComponent,
    SignupComponent,
    FieldComponent,
    DashboardComponent,
    IndicatorComponent,
    HomeComponent,
    NavBarComponent,
    SidenavComponent, GroupListComponent, GroupDetailComponent, SensorTypeCardComponent, SensorTypeCollapsibleComponent, ProgressIndicatorComponent, ServiceWorkerComponent, NotificationsCheckboxComponent, NotificationCenterComponent, CollectionComponent, ConfigurationComponent, UserModifyComponent, GroupExitComponent, NotificationSubscribeComponent, GroupConfComponent, GroupUserListComponent, ThingQuickUpdateComponent, GroupReportsComponent, GroupNavbarComponent, ThingReportsComponent, DateRangePickerComponent, DocsIndexComponent, NewThingComponent, RamCollapsibleComponent, ReportsIndexComponent, StatisticsComponent, ThingStatisticsComponent, ThingGeneralStatisticsComponent, SimpleCardComponent, IncidentChartComponent, RecurrentDaysChartComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MaterializeModule,
    ReactiveFormsModule,
    routing,
    ChartsModule,
    HighlightJsModule,
  ],
  providers: [
    appRoutingProviders,
    SailsService,
    AuthService,
    ThingService,
    GroupService,
    UserService,
    ServiceWorkerService,
    DeviceService,
    NotificationService,
    NavbarService,
    HighlightJsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
