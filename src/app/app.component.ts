
import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { ServiceWorkerService } from './services/service-worker.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private authService: AuthService, private sw: ServiceWorkerService) {  
    this.sw.registerServiceWorker();
  }

  

  title = 'app works!';
}
