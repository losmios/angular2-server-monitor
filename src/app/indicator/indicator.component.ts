import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'indicator',
	templateUrl: './indicator.component.html',
	styleUrls: ['indicator.component.scss']
})
export class IndicatorComponent implements OnInit {

	@Input() name:string;

	@Input() value: string | number;

	@Input() color: string;

	constructor() { }

	ngOnInit() { 

		if(!this.color){
			this.color = 'black';
		}
	 
	}
}