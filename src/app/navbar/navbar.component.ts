import { Component, OnInit, AfterContentInit, ElementRef } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NavbarService } from '../services/navbar.service';

declare let $: any;

@Component({
	selector: 'navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['navbar.component.scss'],
})
export class NavBarComponent implements OnInit {

	isAuth: boolean;

	constructor(
		private authService: AuthService,
		private navbarService: NavbarService
	) { }

	ngOnInit() {
	}


}