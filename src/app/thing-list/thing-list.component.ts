import { Component, OnInit } from '@angular/core';
import { ThingService } from '../services/thing.service';

@Component({
  selector: 'app-thing-list',
  templateUrl: './thing-list.component.html',
  styleUrls: ['./thing-list.component.css']
})
export class ThingListComponent implements OnInit {

  
  things: any;
  error: any;

  constructor(
    private _thingService: ThingService) { }

  ngOnInit() {

    this._thingService.getThings().subscribe(
      things => {
        this.things = things;
      }
    )

  }

}
