import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ThingService } from '../services/thing.service';

import { Group, User } from '../server-monitor.models';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-group-conf',
  templateUrl: './group-conf.component.html',
  styleUrls: ['./group-conf.component.scss']
})
export class GroupConfComponent implements OnInit {

  group: Group
  group$ = new Subject<Group>();


  constructor(
    private route: ActivatedRoute,
    private thingService: ThingService
  ) { }

  ngOnInit() {
    this.route.parent.params
      .subscribe(
      (params: Params) => {
        let id: number = +params['id']; //(+) convertis string 'id' to a number
        this.thingService.getGroup(id)
          .subscribe(
          group => {
            this.group = group;
            this.group$.next(group);
          }
          )

      }
      )
  }



}
