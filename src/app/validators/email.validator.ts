import { FormControl } from '@angular/forms';
import { AuthService } from '../services/auth.service';


export function emailValidator(c) {
	let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

	return EMAIL_REGEXP.test(c.value) ? null : {
		validateEmail: {
			valid: false
		},
		message: 'Email invalido'
	}
}



export function emailRepeatedValidator(
	authService: AuthService
	): (c: FormControl) => Promise<any>{
		return function(c: FormControl){

			return new Promise((resolve, reject) => {

				authService.emailExists(c.value)
					.subscribe(
						exists => {

							if(exists){
								resolve({
									message: 'Email ya existe',
									emailRepeatedValidator:{
										valid: false
									}
								})
							}else{
								resolve(null)
							}
						}, error => {
							reject('Error finding out if the email was repeated');
						}
					)
			})
		}
			
}