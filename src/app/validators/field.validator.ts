import { FormControl, AbstractControl, Validator } from '@angular/forms';
import * as moment from 'moment';


export function sameAsValidator(same: AbstractControl, fieldName: string) {
	return function (c: FormControl) {
		if (c.value === same.value) {
			return null;
		} else {
			return {
				sameAsValidator: {
					valid: false
				},
				message: 'Este campo debe ser igual a ' + fieldName
			}
		}
	}
}

export function onlyLetters(c: FormControl) {
	let letters = new RegExp('^[a-zA-Z ñüáéíóú]+$')
	if (letters.test(c.value)) {
		return null;
	} else {
		return {
			onlyLettersValidator: {
				valid: false
			},
			message: 'Solo se permiten letras en este campo'
		}
	}
}

export function afterDate(since: AbstractControl, fieldName?: string) {
		return function (to: FormControl) {
		let sinceDate = moment(since.value);
		let toDate = moment(to.value);
		if (sinceDate.isBefore(toDate)) {
			return null;
		} else {
			return {
				dateAfterValidator: {
					valid: false
				},
				message: 'Esta fecha debe ser posterior a ' + (fieldName || ' a la fecha de inicio')
			}
		}
		}
}

export function isDate(date: FormControl) {
		let m = moment(date.value);
		if (m.isValid()) {
		return null;
		} else {
		return {
			isDate: {
				valid: false,
				invalidAt: m.invalidAt(),
				parsingFlags: m.parsingFlags()
			}
		}
		}
}

