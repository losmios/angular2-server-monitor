import { Component, OnInit, AfterViewInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { SERVER_URL } from '../../server-url';
import { AuthService } from '../../services/auth.service';
import {ThingService} from "../../services/thing.service";

declare var $: any;
declare var hljs: any;

@Component({
  selector: 'app-new-thing',
  templateUrl: './new-thing.component.html',
  styleUrls: ['./new-thing.component.scss']
})
export class NewThingComponent implements OnInit, AfterViewInit {

  groups: Array<any>;
  selectedGroupId: number

  thingConf =
  {
    "email": "nltobon@gmail.com",
    "password": "Coloca tu contraseña aqui",
    "base_url": "http://localhost:1337",
    "user_id": 1,
    "lang": "spa", // eng || spa,
    "thing": {
      "name": "",
      "group" : this.selectedGroupId
    }
  }


  constructor(
    private navbarService: NavbarService,
    private authService: AuthService,
    private thingService: ThingService
  ) { }

  ngOnInit() {
    this.navbarService.setTitle('Agregar Unidad')
    this.thingConf.base_url = SERVER_URL;
    this.authService.getUser().subscribe(
      user => {
        if (!user) {
          return;
        }
        this.thingConf.email = user.email;
        this.thingConf.user_id = user.id;
      }
    )
    this.thingConf.user_id = this.authService.getId();
    this.thingService.getAdminGroups().subscribe(
      groups => {
        console.debug('Admin groups: ' + groups)
        this.groups = groups;
      },
      error => {
        console.error(error);
      }
    )


  }

  groupSelected(event){
    this.thingConf.thing.group = +event.target.value;
  }

  highlight() {
    $('pre code').each(function (i, block) {
      hljs.highlightBlock(block);
    });
  }

  ngAfterViewInit() {
    //this.highlight();
  }

}
