import {
  Component,
  OnInit,
  Input,
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/core';
import { Sensor } from '../server-monitor.models';
import { ThingService } from '../services/thing.service';

@Component({
  selector: 'app-sensor-type-collapsible',
  templateUrl: './sensor-type-collapsible.component.html',
  styleUrls: ['./sensor-type-collapsible.component.scss'],
  animations: [
    trigger('listState', [
      state('hidden', style({
        height: 0
      })),
      state('shown', style({
        height: '*'
      })),
      transition('hidden <=> shown', animate('500ms ease-out'))
    ])
  ]
})
export class SensorTypeCollapsibleComponent implements OnInit {

  @Input()
  sensors: Sensor[] = [];

  type: string;
  unit: string;
  listState: string = 'hidden';

  constructor(
    private thingService: ThingService
  ) { }

  ngOnInit() {
    this.type = this.sensors[0].type;
    this.unit = '%';
    this.thingService.getSensorObservable()
      .subscribe(sensor => {
        if (!(sensor => sensor.id in this.sensors.map(s => s.id))) {
          return;
        }
        this.sensors = this.sensors.map(s => {
          if (s.id === sensor.id) {
            return Object.assign(sensor, s);
          }
          return s;
        })
      }, error => {
        console.error('Error getting the sensor sockets', error);
      })
  }

  average(): string {
    let sum = 0;
    this.sensors.map(
      s => {
        sum += s.lastMeasure.value;
      }
    )
    let avg = sum / this.sensors.length
    return avg.toFixed(2);
  }

  toggleList() {
    if (this.listState === 'hidden') {
      this.listState = 'shown';
    } else {
      this.listState = 'hidden';
    }
  }

}
