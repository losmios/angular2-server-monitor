import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ThingService } from '../services/thing.service';
import { Subject } from 'rxjs';

//TODO: add to authService redirect url


@Injectable()
export class CanEditGroupGuard implements CanActivate {


	constructor(
		private thingService: ThingService,
		private router: Router) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		console.log('Can edit group guard triggered');
		let subject = new Subject<boolean>();
		if (!('id' in route.params)) {
			subject.next(false);
			subject.complete();
		}
		let groupId = +route.params['id'];
		if (!groupId) {
			groupId = +route.parent.params['id'];
		}
		this.thingService.getUserPermissions(groupId).subscribe(
			permissions => {
				if (permissions.find(p => p === 'admin' || p === 'write')) {
					subject.next(true);
				} else {
					subject.next(false);
				}
				subject.complete();
			}, error => {
				console.error('Error getting the user permissions', error);
			}
		)
		return subject.asObservable();
	}
}