import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ThingService } from '../services/thing.service';
import { Subject } from 'rxjs';

//TODO: add to authService redirect url


@Injectable()
export class HasGroupGuard implements CanActivate{

	constructor(
		private router: Router,
		private thingService: ThingService
		) { }

		

		canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
			console.debug('HasGroupGuard Activated');
			let subject = new Subject<boolean>();
			this.thingService.getJoinedGroups().subscribe(
				groups => {
					if(groups.length > 0){
						subject.next(true)
					}else{
						
						this.router.navigate(['/findgroup']);
						subject.next(false)
					}
					subject.complete();
				}
			)
			return subject.asObservable();
			/*
			console.log('Has Group Guard triggered');	
			if(this.thingService.hasGroups()){
				return true
			}else{
				this.router.navigate(['/findgroup'])
				return false
			}*/
		}
}