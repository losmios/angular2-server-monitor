import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';

//TODO: add to authService redirect url


@Injectable()
export class AuthGuard implements CanActivate{

	isAuth:boolean = false;

	constructor(
		private authService: AuthService,
		private router: Router) { 
			this.authService.isAuth().subscribe(
				ok => this.isAuth = ok
			)
		 }

		

		canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
			console.log('AuthGuard triggered');		
			if(this.isAuth){
				return true;
			}else{
				this.authService.redirectUrl = state.url;
				this.router.navigate(['/login']);
				return false;
			}
		}
}