import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ThingService } from '../services/thing.service';
import { AuthService } from '../services/auth.service';
import { Subject } from 'rxjs';

//TODO: add to authService redirect url
declare var Materialize: any;

@Injectable()
export class IsMemberGuard implements CanActivate {

	constructor(
		private router: Router,
		private thingService: ThingService,
		private authService: AuthService
		) { }



		canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		console.debug('IsMemberGuard Activated');
		let subject = new Subject<boolean>();
		let groupId = +route.params['id'];
		if (!groupId) {
			groupId = +route.parent.params['id'];
		}

		this.thingService.getGroupMembers(groupId).subscribe(
			members => {
				let member = members.find(m => m.id === this.authService.getId());
				if (!member || member.status === 'joining' || member.status === 'blocked') {
					Materialize.toast('Aun no es miembro de este grupo comuniquese con el administrador del grupo');
					subject.next(false);
				} else {
					subject.next(true);
				}
				subject.complete();
			}, error => {
				subject.next(false);
				subject.complete();
				console.error('Connection error on IsMemberGuard');
			}
		)
		return subject.asObservable();
		}
}