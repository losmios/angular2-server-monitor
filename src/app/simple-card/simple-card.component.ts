import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-simple-card',
  templateUrl: './simple-card.component.html',
  styleUrls: ['./simple-card.component.scss']
})
export class SimpleCardComponent implements OnInit {

  @Input()
  name: string;

  @Input()
  value: string | number;

  @Input()
  color: string = 'white';

  @Input()
  unit: string;

  constructor() { }

  ngOnInit() {
  }

}
