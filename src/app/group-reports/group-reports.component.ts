import { Component, OnInit } from '@angular/core';
import { ThingService } from '../services/thing.service';
import { Route, Params, ActivatedRoute } from '@angular/router';
import { Group, Thing } from '../server-monitor.models';

@Component({
  selector: 'app-group-reports',
  templateUrl: './group-reports.component.html',
  styleUrls: ['./group-reports.component.scss']
})
export class GroupReportsComponent implements OnInit {

  group: Group
  things: Thing[]
  from: Date
  to: Date

  constructor(
    private thingService: ThingService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.from = params['from'] || null;
      this.to = params['to'] || null;
    });
    this.route.parent.parent.params.subscribe(
      (params: Params) => {
        let id = +params['id'];
        if (!id) {
          console.error('no id provided in reports');
        }
        this.thingService.getGroup(id).subscribe(
          group => {
            this.group = group;
            this.things = group.things;
          }
        )
      }
    )
  }

}
