import { Component, OnInit } from '@angular/core';
import { ThingService } from '../services/thing.service';
import { NavbarService } from '../services/navbar.service';


@Component({
	selector: 'dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	online: number = 0;
	offline: number = 0;
	onlineChart: {
		data: any,
		labels: any,
		type: string
	}

	constructor(
		private thingService: ThingService,
		private navbarService: NavbarService
	) { }

	ngOnInit() {
		this.navbarService.setTitle('Grupos')
		this.onlineChart = {
			labels: ['Online', 'Offline'],
			data: [this.online, this.offline],
			type: 'doughnut'
		}
		this.thingService.getThings().subscribe(
			things => {
				if (!things) {
					return;
				}
				let total = things.length;
				this.online = things.filter(t => t.alert === false).length;
				this.offline = total - this.online;
				console.debug('online', this.online);
				console.debug('offline', this.offline)
				this.onlineChart.data = [this.online, this.offline];
			}
		)
	}

}