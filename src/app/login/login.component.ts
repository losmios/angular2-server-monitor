import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { emailValidator } from '../validators/email.validator';
import { BehaviorSubject } from 'rxjs';

declare let Materialize: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  sendingForm = new BehaviorSubject<boolean>(false);
  loginForm: FormGroup;
  error: string = "";


  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.authService.isAuth().subscribe(
      ok => {
        if (ok) {
          console.log('logged in', ok);
          let redirectUrl = this.authService.redirectUrl ? this.authService.redirectUrl : '/dashboard';
          this.router.navigate([redirectUrl]);
        }
      }
    )

    this.loginForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        emailValidator,
        Validators.maxLength(200),
        Validators.minLength(5)
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(50)
      ]]
    })

    console.log(this.loginForm);

  }

  login(form) {
    if (!this.loginForm.valid) {
      return
    }
    this.sendingForm.next(true);
    this.authService.login(form.email, form.password)
      .subscribe(
      auth => {

        if (auth) {
          this.loginForm.reset();
        } else {
          setTimeout(() => {
            this.sendingForm.next(false);
          }, 250);

          let message = `
              Correo o Contraseña incorrecta
            `
          Materialize.toast(message, 5000)
          this.error = "Correo o Contraseña incorrecta"
        }
        console.log('this should be false', this.sendingForm);
      },
      error => {
        console.log('error', error);
        setTimeout(() => {
          this.sendingForm.next(false);
        }, 250);
        console.log('this should be false', this.sendingForm);
        if (error.hasOwnProperty('type') && error.type === 'connection') {
          Materialize.toast('Error de Conexion', 5000);
        } else {
          Materialize.toast('Error en login', 5000);
        }

      }
      )
  }

  TBD() {
    Materialize.toast('Esta funcion esta por ser implementada', 3000)
  }




}
