import { 
  Component, 
  OnInit, 
  Input, 
  ContentChild
 } from '@angular/core';
import { FormControlName } from '@angular/forms';

@Component({
	selector: 'field',
	templateUrl: './field.component.html',
	styleUrls: ['./field.component.css']

})
export class FieldComponent implements OnInit {

	@Input()
	label: string;

	@Input()
	feedback: boolean;

	@ContentChild(FormControlName) state;

	constructor() { }

	ngOnInit() { 
    /*this.state.ngOnChanges(changes => {
      console.log(changes);
    })*/
	 }

	 isStateNotValid() {
    return this.label && this.state && !this.state.valid
       && !this.state.control.pending;
  }

  isFeedbackValid() {
    return this.state && this.feedback &&
       !this.state.control.pending && this.state.valid;
  }

  isFeedbackNotValid() {
    return this.state && this.feedback &&
       !this.state.control.pending && !this.state.valid;
  }

  isFeedbackPending() {
    return this.state && this.feedback && this.state.control.pending;
  }
}