import { Component, OnInit } from '@angular/core';

declare let $ :any;

@Component({
	selector: 'home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	moisesito: string;
	counter: number;

	//Deprecated
	homeClick(){
		$('.home').on('click', () => {
			alert('hello');
		})
	}
	
	sendAlert(){
		alert('hello using angular');
	}

	count(){
		this.counter++;
	}

	getPower(base, exponent){
		return Math.pow(base, exponent);
	}

	constructor() { }

	ngOnInit() { 
		this.counter = 0; //Initialize the counter
		this.moisesito = 'Hello this works for Moises';
		//this.homeClick();
		/* Tambien puedes utilizar las directivas de angular para los eventos
		por ejemplo para el evento click usas (click)="sendAlert()"
		revisa el html
		*/
	}
}

