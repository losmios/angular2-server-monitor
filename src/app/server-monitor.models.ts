import { Observable } from 'rxjs';


interface SailsObject {
    id?: number,
    createdAt?: Date,
    updatedAt?: Date,
    url?: string
}

export interface User extends SailsObject {
    firstName?: string
    lastName: string
    email: string
    password: string
    auth?: any
}

export interface Sensor {
    id: number
    name: string
    alert?: boolean
    thing: number
    type: string
    measures: Array<any>
    lastMeasure?: any
    unit?: any
    createdAt: Date
    updatedAt: Date

}

export interface Incident extends SailsObject {
    sensor: Sensor | number
    thing: Thing | number
    group: Group | number
    type: string
}

export interface Measure extends SailsObject {
    value: number
    sensor: number
    unit: number
}

export interface Thing {
    id: number
    name?: string
    domain?: string
    ip?: string
    mac?: string
    alert?: boolean
    timeout?: number
    refreshRate?: number
    sensors: Array<Sensor>
    group: Group
    onlineTime: string
    createdAt: Date
    updatedAt: Date
}

export interface Group extends SailsObject {
    name: string,
    things?: Thing[] | Observable<Thing[]>,
    users?: User[]
}

export interface Unit extends SailsObject {
    name: string
    type: string
    abbreviation: string
}
