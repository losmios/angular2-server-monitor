import {
  Component,
  OnInit,
  Input,
  EventEmitter
} from '@angular/core';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { isDate, afterDate } from '../validators/field.validator';
import { Thing, Measure, Sensor } from '../server-monitor.models';
import { ThingService } from '../services/thing.service';
import * as moment from 'moment';
import { MaterializeAction } from 'angular2-materialize';




declare var Materialize: any;

@Component({
  selector: 'app-thing-reports',
  templateUrl: './thing-reports.component.html',
  styleUrls: ['./thing-reports.component.scss'],
})
export class ThingReportsComponent implements OnInit {

  @Input()
  thing: Thing

  @Input('from')
  defaultFrom: Date

  @Input('to')
  defaultTo: Date

  measures: Array<Measure>
  sensorTypes: Array<any> = []
  chartsData: any
  chartLineType = 'line';
  chartOptions = {}

  modalActions = new EventEmitter<string | MaterializeAction>();


  pickadateOptions = {
    max: new Date(),
    closeOnSelect: true,
    today: 'Hoy',
    clear: 'Borrar',
    close: 'Cerrar'
  }

  datepairForm: FormGroup

  constructor(
    private thingService: ThingService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    if (!this.thing) {
      console.error('thing not provided for reports')
    }
    this.thingService.getThings().subscribe(
      things => {
        this.thing = things.find(t => t.id === this.thing.id)
        if (!this.thing) {
          console.error('thing not found in service')
        }
        let from: moment.Moment = null;
        if (this.defaultFrom) {
          from = moment(this.defaultFrom).subtract(2, 'h');
        }
        let to: moment.Moment = null;
        if (this.defaultTo) {
          to = moment(this.defaultTo);
        } else if (this.defaultFrom) {
          to = moment(this.defaultFrom).add(2, 'h');
        }
        this.getMeasures(from, to);

      }
    )

    this.datepairForm = this.formBuilder.group({
      since: ['', [
        Validators.required,
        isDate
      ]
      ]
    })
    this.datepairForm.addControl(
      'to',
      new FormControl('', [
        Validators.required,
        isDate,
        afterDate(this.datepairForm.get('since'), 'Desde')
      ])
    )
  }

  getMeasures(since?: moment.Moment, to?: moment.Moment) {
    let sinceDate = null
    let toDate = null
    if (since) {
      if (moment(since).isValid()) {
        sinceDate = moment(since).toDate();
      }
      if (moment(to).isValid()) {
        toDate = moment(to).toDate();
      } else {
        toDate = moment().toDate();
      }

    }
    this.thingService.getMeasures(this.thing.sensors.map(s => s.id), sinceDate, toDate).subscribe(
      (measures: Measure[]) => {
        this.thing.sensors.map(
          sensor => {
            sensor.measures = measures.filter(m => m.sensor === sensor.id)
          }
        )


        this.loadTypes(this.thing.sensors);
        this.loadChartData();
      }, error => {
        console.error('error getting the measures', error);
      }
    )
  }

  loadTypes(sensors: Sensor[]) {
    //I don't want to plot the total and the percentage
    sensors = sensors.filter(sensor => {
      if (sensor.type === 'ram') {
        if (sensor.name === 'total' || sensor.name === 'percent' || sensor.name.toLowerCase() === 'porcentaje') {
          return false;
        }
      }
      return true;
    });
    sensors.map(s => {
      if (s['type'] === 'ram') {
        console.debug('Sensor: ', s);
      }
      if (!this.sensorTypes.find(t => t['type'] === s['type'])) {

        this.sensorTypes.push({
          'type': s['type'],
          sensors: sensors.filter(aux => aux['type'] === s['type'])
        })
      }
    })
    console.debug('Sensor types:', this.sensorTypes);
  }

  loadChartData() {
    this.sensorTypes = this.sensorTypes.map(
      st => {
        st['datasets'] = [];
        st['averages'] = {
          data: [],
          labels: []
        }
        if (!st.sensors.length) {
          console.error('no sensors of type ' + st['type'] + ' this error should not be thrown, please verify', st);
          return;
        }
        if (!st.sensors[0].measures.length) {
          console.debug('No measures for sensor ' + st.sensors[0].id)
          st['noData'] = true
          return st
        }
        let first = moment(st.sensors[0].measures[0].createdAt)
        let last = moment(st.sensors[0].measures[st.sensors[0].measures.lenght - 1])
        if (first.day() === last.day()) {
          st['labels'] = st.sensors[0].measures.map(m => moment(m.createdAt).format('h:mm:ss a'))
        } else {
          st['labels'] = st.sensors[0].measures.map(m => moment(m.createdAt).format('DD/MM h:mm'))
        }

        st.sensors.map(s => {
          let sum = 0;
          let data = {
            'label': s.name,
            'data': s.measures.map(m => {
              sum += m.value;
              return m.value
            }),
            'fill': false
          }
          let average = {
            data: [Math.round((sum / s.measures.length) * 100) / 100],
            label: s.name
          }
          st['datasets'].push(data);
          st['averages']['data'].push(average);
          st['averages']['labels'].push(s.name);
        })
        st['noData'] = false;
        return st;
      }
    )
    console.debug('Sensor types with chart data', this.sensorTypes);
  }

  setDates(since: string, to: string, type?: string) {
    if (type) {
      if (type === 'today') {
        return this.getMeasures();
      } else if (type === 'week') {
        return this.getMeasures(moment().subtract(1, 'week'), moment());
      }
    }
    if (!this.datepairForm.valid) {
      Materialize.toast('Intervalo de fecha invalido', 3000)
      return
    }
    this.getMeasures(moment(since), moment(to));
  }

  openModal() {
    this.modalActions.emit({ action: "modal", params: ['open'] });
  }
  closeModal() {
    this.modalActions.emit({ action: "modal", params: ['close'] });
  }


}
