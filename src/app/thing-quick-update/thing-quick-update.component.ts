import {
  Component,
  OnInit,
  Input,
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/core';
import { Thing } from '../server-monitor.models';
import { ThingService } from '../services/thing.service';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var Materialize: any;

@Component({
  selector: 'app-thing-quick-update',
  templateUrl: './thing-quick-update.component.html',
  styleUrls: ['./thing-quick-update.component.scss'],
  animations: [
    trigger('editState', [
      state('hidden', style({
        height: 0
      })),
      state('shown', style({
        height: '*'
      })),
      transition('hidden <=> shown', animate('500ms ease-out'))
    ])
  ]
})
export class ThingQuickUpdateComponent implements OnInit {

  @Input()
  thing: Thing

  @Input()
  editState: string

  updateForm: FormGroup;
  sendingForm = false
  success = false

  constructor(
    private thingService: ThingService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.updateForm = this.formBuilder.group({
      name: [this.thing.name, [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(255)
      ]],
      timeout: [(this.thing.timeout / 1000), [
        Validators.required
      ]],
      timeoutUnit: [1000, [
        Validators.required
      ]],
      refreshRate: [(this.thing.refreshRate / 1000), [
        Validators.required
      ]],
      refreshRateUnit: [1000, [
        Validators.required
      ]]
    })
  }

  updateThing(formData: {
    name: string
    timeout: number
    timeoutUnit: number
    refreshRate: number
    refreshRateUnit: number
  }) {
    if (!this.updateForm.valid || !this.updateForm.dirty) {
      return;
    }
    this.sendingForm = true;
    let data = {
      name: formData.name,
      timeout: formData.timeout * formData.timeoutUnit,
      refreshRate: formData.refreshRate * formData.refreshRateUnit
    }
    this.thingService.updateThing(this.thing.id, data).subscribe(
      thing => {
        this.thing = thing;
        this.success = true;
        setTimeout(() => this.success = false, 2000);
        Materialize.toast('Informacion guardada', 2000);
        Materialize.toast('Para que la tasa de refresco se aplique debe esperar 10 segundos', 10000);
        this.sendingForm = false;
      }, error => {
        this.sendingForm = false;
        Materialize.toast('Error actualizando la unidad', 2000);
        Materialize.toast(error);
      }

    )
  }

}
