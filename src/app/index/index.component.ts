import { Component } from '@angular/core';
import { NavbarService } from '../services/navbar.service';

declare var $: any;

@Component({
	selector: 'index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.css']
})
export class IndexComponent {

	images: any = {
		fondoAliados: "./assets/img/fondo_aliados.jpg",
		grupos: "./assets/img/analytic-monitoring.jpg",
		google_seo: "assets/img/controlar.png",
		edit_blue: "assets/img/dispositivos.png",
		icono_online: "assets/img/ebook.png",
		pross: "assets/img/informacion.png",
		bond: "./assets/img/ordenador.png",
		ordenador: "./assets/img/ordenador1.png",
		second: "./assets/img/pantallas.jpg",
		avatar: "./assets/img/avatar1.png",
		avatar2: "./assets/img/avatar2.png",
		avatar3: "./assets/img/avatar3.png",
		avatar4: "./assets/img/avatar4.png",
		favicon: "./assets/img/favicon (9).ico",
		urbe: "./assets/img/urbe.png"

	}



	constructor(
		private navbarService: NavbarService
	) { }

	ngOnInit() {
		this.navbarService.setTitle('Monitor');
		$('.parallax').parallax();
		$('.head-link').click(function (e) {
			e.preventDefault();

			var goto = $(this).attr('href');

			$('html, body').animate({
				scrollTop: $(goto).offset().top
			}, 800);
		});
	}


}