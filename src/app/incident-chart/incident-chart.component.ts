import { Component, OnInit, Input } from '@angular/core';
import { Sensor, Incident } from '../server-monitor.models';
import { ThingService } from '../services/thing.service';

@Component({
  selector: 'app-incident-chart',
  templateUrl: './incident-chart.component.html',
  styleUrls: ['./incident-chart.component.scss']
})
export class IncidentChartComponent implements OnInit {

  @Input()
  sensor: Sensor;
  incidents: Incident[];
  chart: any;
  message: string;

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(
    private thingService: ThingService
  ) { }

  ngOnInit() {
    console.debug('finding the incidents for: ', this.sensor);
    this.thingService.getIncidents({
      sensor: this.sensor.id
    }).subscribe(
      incidents => {
        this.incidents = incidents;
        if (!incidents.length) {
          this.message = `No hay incidencias`;
        }
        this.chart = this.loadChartData(this.incidents);
      }, error => {
        console.error('Error getting the incidents for sensor: ', this.sensor);
      });
  }

  loadChartData(incidents: Incident[]) {
    return {
      type: 'line',
      data: {
        labels: incidents.map(inc => inc.createdAt),
        datasets: [{
          label: this.sensor.name,
          data: incidents.map(inc => 1)
        }]
      },
      options: {
        showLines: false
      }
    }
  }

}
