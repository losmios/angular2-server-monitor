import { Component, OnInit, Input } from '@angular/core';
import { Thing } from '../server-monitor.models';

@Component({
  selector: 'app-thing-general-statistics',
  templateUrl: './thing-general-statistics.component.html',
  styleUrls: ['./thing-general-statistics.component.scss']
})
export class ThingGeneralStatisticsComponent implements OnInit {

  @Input()
  thing: Thing

  constructor() { }

  ngOnInit() {
    if (!this.thing) {
      console.error('No thing provided for ThingGeneralStatisticsComponent', this.thing);
    }
  }

}
