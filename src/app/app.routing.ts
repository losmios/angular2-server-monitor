import { Routes, RouterModule } from '@angular/router';

import { ModuleWithProviders } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { ThingListComponent } from './thing-list/thing-list.component';
import { GroupFormComponent } from './group-form/group-form.component';
import { IndexComponent } from './index/index.component';
import { SignupComponent } from './signup';
import { DashboardComponent } from './dashboard';
import { HomeComponent } from './home-moises/home.component';
import { GroupDetailComponent } from './group-detail/group-detail.component';
import { NotificationCenterComponent } from './notification-center/notification-center.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { UserModifyComponent } from './user-modify/user-modify.component';
import { GroupConfComponent } from './group-conf/group-conf.component';
import { GroupReportsComponent } from './group-reports/group-reports.component';
import { GroupNavbarComponent } from './group-navbar/group-navbar.component';
import { DocsIndexComponent } from './docs/docs-index/docs-index.component';
import { NewThingComponent } from './docs/new-thing/new-thing.component';
import { ReportsIndexComponent } from './reports-index/reports-index.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { ThingStatisticsComponent } from './thing-statistics/thing-statistics.component';

import { AuthGuard } from './routing/auth.guard';
import { HasGroupGuard } from './routing/has-group.guard';
import { CanEditGroupGuard } from './routing/can-edit-group.guard';
import { IsMemberGuard } from './routing/is-member.guard';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'things',
        component: ThingListComponent,
        canActivate: [AuthGuard]
    }, {
        path: 'findgroup',
        component: GroupFormComponent,
        canActivate: [AuthGuard]
    }, {
        path: '',
        component: IndexComponent
    }, {
        path: 'signup',
        component: SignupComponent
    }, {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard, HasGroupGuard]
    }, {
        path: 'home-moises',
        component: HomeComponent
    },
    {
        path: 'group/:id',
        component: GroupNavbarComponent,
        canActivate: [AuthGuard, HasGroupGuard, IsMemberGuard],
        children: [
            {
                path: 'reports',
                component: ReportsIndexComponent,
                children: [
                    {
                        path: '',
                        component: GroupReportsComponent,
                        resolve: {
                            dates: 'dates'
                        }
                    }, {
                        path: 'statistics',
                        component: StatisticsComponent,
                    }, {
                        path: 'statistics/:thingId',
                        component: ThingStatisticsComponent
                    }
                ]
            }, {
                path: '',
                component: GroupDetailComponent
            }, {
                path: 'configuration',
                component: GroupConfComponent,
                canActivate: [CanEditGroupGuard]
            }
        ]
    }, {
        path: 'notifications',
        component: NotificationCenterComponent,
        canActivate: [AuthGuard, HasGroupGuard]
    }, {
        path: 'profile',
        component: UserModifyComponent,
        canActivate: [AuthGuard]
    }, {
        path: 'documentation',
        component: DocsIndexComponent,
        children: [
            {
                path: '',
                component: NewThingComponent
            }
        ]
    }
]

export const appRoutingProviders: any[] = [
    [AuthGuard, HasGroupGuard, CanEditGroupGuard, IsMemberGuard, {
        provide: 'dates',
        useValue: (from: Date, to: Date) => {
            return {
                from: from, to: to
            }
        }
    }]
]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes)