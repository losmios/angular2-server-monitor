import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

interface CollectionLink {
  name: string
  url?: string
  color?: string
}

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  @Input() array: Array<CollectionLink>
  @Input() defaultColor: string
  @Input() header: string
  @Input() clickHandle: (any) => void
  @Input() redirect = true

  @Output() select: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  clickEvent(ev) {
    console.log('item selected', ev);
    this.select.emit(ev);
    if (this.clickHandle) {
      this.clickHandle(ev);
    }
    if (this.redirect && 'url' in ev && ev.url) {
      this.router.navigate([ev.url], { relativeTo: this.route });
    }
  }

  ngOnInit() {
    if (!this.array) {
      return;
    }
    this.array = this.array.map(a => {
      if (!('url' in a)) {
        a.url = null;
      } else if (!a.url) {
        a.url = null;
      }
      return a;
    })
  }

  isRead(item: any) {
    if (!('read' in item)) {
      return false;
    }
    if (item.read) {
      return true;
    }
    return false;
  }

}
