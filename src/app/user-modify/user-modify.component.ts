import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavbarService } from '../services/navbar.service';

declare var Materialize: any;

@Component({
  selector: 'app-user-modify',
  templateUrl: './user-modify.component.html',
  styleUrls: ['./user-modify.component.scss']
})
export class UserModifyComponent implements OnInit {

  user: any
  userForm: FormGroup;
  sendingForm: boolean;
  loading: boolean = true;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private navbarService: NavbarService
  ) { }

  ngOnInit() {
    this.sendingForm = false;
    this.navbarService.setTitle('Modificar Usuario');
    this.authService.getUser().subscribe(
      user => {
        if (!user) {
          return;
        }
        this.user = user;
        this.userForm = this.formBuilder.group({
          firstName: [
            this.user.firstName || '',
            [
              Validators.required,
              Validators.minLength(4),
              Validators.maxLength(35)
            ]
          ],
          lastName: [this.user.lastName || '', [
            Validators.required,
            Validators.minLength(4),
            Validators.maxLength(35)
          ]]
        })
        this.loading = false;
      })

  }

  updateUser(user: {
    firstName: string,
    lastName: string
  }) {
    if (!this.userForm.valid || !this.userForm.dirty) {
      return;
    }
    this.sendingForm = true;
    this.authService.update(user)
      .subscribe(
      user => {
        this.sendingForm = false;
        Materialize.toast('Usuario actualizado', 2000);
        //this.userForm.reset();
      }, error => {
        Materialize.toast(error, 3000);
      }
      )
  }

}
