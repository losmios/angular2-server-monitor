import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ThingService } from '../services/thing.service';
import { NavbarService } from '../services/navbar.service';
import { Group } from '../server-monitor.models';

@Component({
  selector: 'app-group-navbar',
  templateUrl: './group-navbar.component.html',
  styleUrls: ['./group-navbar.component.scss']
})
export class GroupNavbarComponent implements OnInit {

  group: Group
  canEdit: boolean = false
  activeRoute: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private thingService: ThingService,
    private router: Router,
    private navbarService: NavbarService
  ) { }

  ngOnInit() {
    if (this.route.children.length > 0) {
      this.activeRoute = false;
    }
    this.route.params
      .subscribe(
      (params: Params) => {
        let id: number = +params['id']; //(+) convertis string 'id' to a number
        this.thingService.getGroup(id)
          .subscribe(
          group => {
            this.group = group;
            this.canEditGroup();
            this.navbarService.setTitle(this.group.name)
          }
          )

      }
      )
  }

  private canEditGroup() {
    this.thingService.getUserPermissions(this.group.id).subscribe(
      permissions => {
        if (permissions.find(p => p === 'admin' || p === 'write')) {
          this.canEdit = true;
        } else {
          this.canEdit = false;
        }
      }
    )
  }

  littleHacky($event) {
    this.activeRoute = false;
  }

  littleHack($envet) {
    this.activeRoute = true;
  }

}
