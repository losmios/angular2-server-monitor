import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ThingService } from '../services/thing.service';
import { Group, Thing } from '../server-monitor.models';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs';

declare let Materialize: any;

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.scss']
})
export class GroupDetailComponent implements OnInit {


  constructor(
    private route: ActivatedRoute,
    private thingService: ThingService,
    private router: Router
  ) { }

  group: Group = {
    name: 'Group Name',
    id: null,
    createdAt: null,
    updatedAt: null,
    things: []
  };

  things: Thing[] = [];

  canEdit: boolean;
  loading: boolean;

  ngOnInit() {
    this.loading = true;
    this.route.params
      .subscribe(
      (params: Params) => {
        let id: number = +params['id']; //(+) convertis string 'id' to a number
        this.thingService.getGroup(id)
          .subscribe(
          group => {
            this.group = group;
            this.loadThings();
            this.canEditGroup();
          }, error => {
            console.error('Error getting the group in the groupCOmponent', error);
          }
          )

      }
      )


  }

  loadThings() {
    this.thingService.getThings()
      .subscribe(
      things => {
        if (things && things.length > 0) {
          this.things = things.filter(t => this.group.id === t.group.id);
          this.things = JSON.parse(JSON.stringify(this.things));
          console.log('the things I have: ', things);

        }
        this.loading = false;
      }
      )
  }

  goToSettings() {
    this.router.navigate(['/group/' + this.group.id + '/configuration']);
  }

  private canEditGroup() {
    this.thingService.getUserPermissions(this.group.id).subscribe(
      permissions => {
        if (permissions.find(p => p === 'admin' || p === 'write')) {
          this.canEdit = true;
        } else {
          this.canEdit = false;
        }
      }, error => {
        console.error('error getting the user permissions', error);
      }
    )
  }

}
