export interface Sensor {
    id: number
    name: string
    alert?: boolean
    thing: number
    measures: Array<any>
    lastMeasure?: any
    unit?: any
    createdAt: Date
    updatedAt: Date

}
