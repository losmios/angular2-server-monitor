import { Component, OnInit, Input } from '@angular/core';
import { Sensor } from '../server-monitor.models';


@Component({
  selector: 'app-sensor-type-card',
  templateUrl: './sensor-type-card.component.html',
  styleUrls: ['./sensor-type-card.component.scss']
})
export class SensorTypeCardComponent implements OnInit {

  @Input()
  sensors: Sensor[] = [];

  unit: string = "%";

  constructor() { }

  average(): string {
    let sum = 0;
    this.sensors.map(
      s => {
        sum += s.lastMeasure.value;
      }
    )
    let avg = sum/this.sensors.length
    return avg.toFixed(2);
  }

  ngOnInit() {
    console.log(this.sensors);
  }

}
