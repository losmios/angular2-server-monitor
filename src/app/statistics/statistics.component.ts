import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ThingService } from '../services/thing.service';
import { Thing } from '../server-monitor.models';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

  things: Thing[];


  constructor(
    private route: ActivatedRoute,
    private thingService: ThingService
  ) { }

  ngOnInit() {

    this.route.parent.parent.params.subscribe(
      (params: Params) => {
        let id = +params['id'];
        if (!id) {
          console.error('No id provided for group', params);
        }
        this.thingService.getThings().subscribe(
          things => {
            things = things.filter(thing => thing.group.id === id);
            this.things = things.map(thing => {
              return Object.assign({}, thing, { url: `${thing.id}` });
            })
          }
        )
      }
    )
  }

}
