let request = require('request');

let sensors = [1, 2, 3];

to = new Date();
since = new Date(to.toJSON().slice(0, 10));

let query = {
	sensor: sensors,
	createdAt: {
		'>': since.toJSON(),
		'<': to.toJSON()
	}
}

query = JSON.stringify(query);
console.log(query);
request(`http://localhost:1337/measure?limit=10000&populate=["unit"]&where=${query}`, (error, response, body) => {
	if (error) {
		console.error(error);
	}
	let measures = JSON.parse(body);
	//console.log(measures);
	let sensors = [];
	console.log(measures.length);
	console.log(measures[0].sensor);
	console.log(measures.filter(m => m.sensor === 1).length);
	console.log(measures.filter(m => m.sensor === 2).length);
	console.log(measures.filter(m => m.sensor === 3).length);
})
